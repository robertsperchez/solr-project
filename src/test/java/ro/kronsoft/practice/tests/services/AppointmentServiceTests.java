package ro.kronsoft.practice.tests.services;

import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import ro.kronsoft.practice.entities.Appointment;
import ro.kronsoft.practice.services.AppointmentService;

@RunWith(PowerMockRunner.class)
@PrepareForTest(fullyQualifiedNames = "ro.kronsoft.practice.services.*")
class AppointmentServiceTests {

	private AppointmentService mockService;

	@Test
	void testServiceMethods() throws Exception {
		mockService = Mockito.mock(AppointmentService.class);

		List<Appointment> appointmentsList = new ArrayList<>();
		Iterable<Appointment> returnedList = new ArrayList<>();
		Appointment appointment = new Appointment();
		Appointment returnedAppointment = new Appointment();
		Long returnedCount;

		// findAll test
		when(mockService.getAllAppointments()).thenReturn(appointmentsList);
		returnedList = mockService.getAllAppointments();
		Assert.assertEquals("Returned list isn't the same as what should be returned!", appointmentsList, returnedList);

		// getAppointmentById test
		when(mockService.getAppointmentById(Mockito.anyString())).thenReturn(appointment);
		returnedAppointment = mockService.getAppointmentById("test-id");
		Assert.assertEquals("Returned object isn't the same as what should be returned!", appointment,
				returnedAppointment);

		// saveAppointment test
		when(mockService.saveAppointment(isA(Appointment.class))).thenReturn(appointment);
		returnedAppointment = mockService.saveAppointment(new Appointment());
		Assert.assertEquals("Returned object isn't the same as what should be returned!", appointment,
				returnedAppointment);

		// deleteAppointment test
		doNothing().when(mockService).deleteAppointment(Mockito.anyString());
		mockService.deleteAppointment("test-id");
		verify(mockService, times(1)).deleteAppointment("test-id");

		// deleteAllAppointments test
		doNothing().when(mockService).deleteAllAppointments();
		mockService.deleteAllAppointments();
		verify(mockService, times(1)).deleteAllAppointments();

		// countAllAppointments test
		when(mockService.countAllAppointments()).thenReturn((Long) 5l);
		returnedCount = mockService.countAllAppointments();
		Assert.assertEquals("Returned count isn't the same as what should be returned!", (Long) 5l, returnedCount);

		// countByPatientId test
		when(mockService.countByPatientId(Mockito.anyString())).thenReturn((Long) 5l);
		returnedCount = mockService.countByPatientId("test-id");
		Assert.assertEquals("Returned count isn't the same as what should be returned!", (Long) 5l, returnedCount);

		// countByDoctorId test
		when(mockService.countByDoctorId(Mockito.anyString())).thenReturn((Long) 5l);
		returnedCount = mockService.countByDoctorId("test-id");
		Assert.assertEquals("Returned count isn't the same as what should be returned!", (Long) 5l, returnedCount);

		// findAllLazyLoading test
		when(mockService.findAllLazyLoading(Mockito.anyInt(), Mockito.anyInt())).thenReturn(appointmentsList);
		returnedList = mockService.findAllLazyLoading(0, 1);
		Assert.assertEquals("Returned list isn't the same as what should be returned!", appointmentsList, returnedList);

		// sortAllLazyLoading test
		when(mockService.sortAllLazyLoading(Mockito.anyInt(), Mockito.anyInt(), isA(Sort.class)))
				.thenReturn(appointmentsList);
		returnedList = mockService.sortAllLazyLoading(0, 1, Sort.by("test").ascending());
		Assert.assertEquals("Returned list isn't the same as what should be returned!", appointmentsList, returnedList);

		// findAllByPatientId test
		when(mockService.findAllByPatientId(Mockito.anyString(), Mockito.anyInt(), Mockito.anyInt()))
				.thenReturn(appointmentsList);
		returnedList = mockService.findAllByPatientId("test-id", 0, 1);
		Assert.assertEquals("Returned list isn't the same as what should be returned!", appointmentsList, returnedList);

		// findAllByPatientId test
		when(mockService.findAllByPatientId(Mockito.anyString(), Mockito.anyInt(), Mockito.anyInt(), isA(Sort.class)))
				.thenReturn(appointmentsList);
		returnedList = mockService.findAllByPatientId("test-id", 0, 1, Sort.by("test").ascending());
		Assert.assertEquals("Returned list isn't the same as what should be returned!", appointmentsList, returnedList);

		// findAllByDoctorId test
		when(mockService.findAllByDoctorId(Mockito.anyString(), Mockito.anyInt(), Mockito.anyInt()))
				.thenReturn(appointmentsList);
		returnedList = mockService.findAllByDoctorId("test-id", 0, 1);
		Assert.assertEquals("Returned list isn't the same as what should be returned!", appointmentsList, returnedList);

		// findAllByDoctorId test
		when(mockService.findAllByDoctorId(Mockito.anyString(), Mockito.anyInt(), Mockito.anyInt(), isA(Sort.class)))
				.thenReturn(appointmentsList);
		returnedList = mockService.findAllByDoctorId("test-id", 0, 1, Sort.by("test").ascending());
		Assert.assertEquals("Returned list isn't the same as what should be returned!", appointmentsList, returnedList);

		// filterAppointments test
		Map<String, Object> filterFields = null;
		Map<String, String> sortFields = null;
		when(mockService.filterAppointments(Mockito.anyMap(), Mockito.anyMap(), isA(Pageable.class)))
				.thenReturn(appointmentsList);
		returnedList = mockService.filterAppointments(filterFields, sortFields, PageRequest.of(0, 5));
		Assert.assertEquals("Returned list isn't the same as what should be returned!", appointmentsList, returnedList);

		// findByDateAndDoctorId test
		when(mockService.findByDateAndDoctorId(Mockito.anyString(), Mockito.anyString())).thenReturn(appointmentsList);
		returnedList = mockService.findByDateAndDoctorId("2020-01-01", "test-id");
		Assert.assertEquals("Returned list isn't the same as what should be returned!", appointmentsList, returnedList);
	}
}
