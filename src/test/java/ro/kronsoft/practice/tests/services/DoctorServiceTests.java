package ro.kronsoft.practice.tests.services;

import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import ro.kronsoft.practice.entities.Doctor;
import ro.kronsoft.practice.entities.enums.SpecialtyType;
import ro.kronsoft.practice.services.DoctorService;

@RunWith(PowerMockRunner.class)
@PrepareForTest(fullyQualifiedNames = "ro.kronsoft.practice.services.*")
class DoctorServiceTests {

	private DoctorService mockService;

	@Test
	void testServiceMethods() throws Exception {
		mockService = Mockito.mock(DoctorService.class);

		List<Doctor> doctorsList = new ArrayList<>();
		Iterable<Doctor> returnedList = new ArrayList<>();
		Doctor doctor = new Doctor();
		Doctor returnedDoctor = new Doctor();
		Long returnedCount;

		// findDoctorById test
		when(mockService.findDoctorById(Mockito.anyString())).thenReturn(doctor);
		returnedDoctor = mockService.findDoctorById("test-id");
		Assert.assertEquals("Returned object isn't the same as what should be returned!", doctor, returnedDoctor);

		// findDoctorByEmail test
		when(mockService.findDoctorByEmail(Mockito.anyString())).thenReturn(doctor);
		returnedDoctor = mockService.findDoctorByEmail("testemail");
		Assert.assertEquals("Returned object isn't the same as what should be returned!", doctor, returnedDoctor);

		// getAllDoctors test
		when(mockService.getAllDoctors()).thenReturn(doctorsList);
		returnedList = mockService.getAllDoctors();
		Assert.assertEquals("Returned list isn't the same as what should be returned!", doctorsList, returnedList);

		// saveDoctor test
		when(mockService.saveDoctor(isA(Doctor.class))).thenReturn(doctor);
		returnedDoctor = mockService.saveDoctor(new Doctor());
		Assert.assertEquals("Returned object isn't the same as what should be returned!", doctor, returnedDoctor);

		// deleteDoctor test
		doNothing().when(mockService).deleteDoctor(Mockito.anyString());
		mockService.deleteDoctor("test-id");
		verify(mockService, times(1)).deleteDoctor("test-id");

		// deleteAllDoctors test
		doNothing().when(mockService).deleteAllDoctors();
		mockService.deleteAllDoctors();
		verify(mockService, times(1)).deleteAllDoctors();

		// countAllDoctors test
		when(mockService.countAllDoctors()).thenReturn((Long) 5l);
		returnedCount = mockService.countAllDoctors();
		Assert.assertEquals("Returned count isn't the same as what should be returned!", (Long) 5l, returnedCount);

		// findByFullNameAndSpecialty test
		when(mockService.findByFullNameAndSpecialty(Mockito.anyString(), Mockito.anyObject())).thenReturn(doctorsList);
		returnedList = mockService.findByFullNameAndSpecialty("Test", SpecialtyType.CARDIOLOGY);
		Assert.assertEquals("Returned list isn't the same as what should be returned!", doctorsList, returnedList);

		// findByFullNameStartingWith test
		when(mockService.findByFullNameStartingWith(Mockito.anyString())).thenReturn(doctorsList);
		returnedList = mockService.findByFullNameStartingWith("Test");
		Assert.assertEquals("Returned list isn't the same as what should be returned!", doctorsList, returnedList);

		// findAllLazyLoading test
		when(mockService.findAllLazyLoading(Mockito.anyInt(), Mockito.anyInt())).thenReturn(doctorsList);
		returnedList = mockService.findAllLazyLoading(0, 1);
		Assert.assertEquals("Returned list isn't the same as what should be returned!", doctorsList, returnedList);

		// sortAllLazyLoading test
		when(mockService.sortAllLazyLoading(Mockito.anyInt(), Mockito.anyInt(), isA(Sort.class)))
				.thenReturn(doctorsList);
		returnedList = mockService.sortAllLazyLoading(0, 1, Sort.by("test").ascending());
		Assert.assertEquals("Returned list isn't the same as what should be returned!", doctorsList, returnedList);

		// filterDoctors test
		Map<String, Object> filterFields = null;
		Map<String, String> sortFields = null;
		when(mockService.filterDoctors(Mockito.anyMap(), Mockito.anyMap(), isA(Pageable.class)))
				.thenReturn(doctorsList);
		returnedList = mockService.filterDoctors(filterFields, sortFields, PageRequest.of(0, 5));
		Assert.assertEquals("Returned list isn't the same as what should be returned!", doctorsList, returnedList);

		// findByEmailNotId test
		when(mockService.findByEmailNotId(Mockito.anyString(), Mockito.anyString())).thenReturn(doctor);
		returnedDoctor = mockService.findByEmailNotId("test-id", "test-id");
		Assert.assertEquals("Returned object isn't the same as what should be returned!", doctor, returnedDoctor);

		// findByFullName test
		when(mockService.findByFullName(Mockito.anyString())).thenReturn(doctor);
		returnedDoctor = mockService.findByFullName("test-id");
		Assert.assertEquals("Returned object isn't the same as what should be returned!", doctor, returnedDoctor);

	}
}
