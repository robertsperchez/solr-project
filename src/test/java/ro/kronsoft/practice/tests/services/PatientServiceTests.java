package ro.kronsoft.practice.tests.services;

import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.data.domain.Sort;

import ro.kronsoft.practice.entities.Patient;
import ro.kronsoft.practice.services.PatientService;

@RunWith(PowerMockRunner.class)
@PrepareForTest(fullyQualifiedNames = "ro.kronsoft.practice.services.*")
class PatientServiceTests {

	private PatientService mockService;

	@Test
	void testServiceMethods() throws Exception {
		mockService = Mockito.mock(PatientService.class);

		List<Patient> patientsList = new ArrayList<>();
		Iterable<Patient> returnedList = new ArrayList<>();
		Patient patient = new Patient();
		Patient returnedPatient = new Patient();
		Long returnedCount;

		// getPatientById test
		when(mockService.getPatientById(Mockito.anyString())).thenReturn(patient);
		returnedPatient = mockService.getPatientById("test-id");
		Assert.assertEquals("Returned object isn't the same as what should be returned!", patient, returnedPatient);

		// findPatientByCnp test
		when(mockService.findPatientByCnp(Mockito.anyString())).thenReturn(patient);
		returnedPatient = mockService.findPatientByCnp("1990101000000");
		Assert.assertEquals("Returned object isn't the same as what should be returned!", patient, returnedPatient);

		// getAllPatients test
		when(mockService.getAllPatients()).thenReturn(patientsList);
		returnedList = mockService.getAllPatients();
		Assert.assertEquals("Returned list isn't the same as what should be returned!", patientsList, returnedList);

		// savePatient test
		when(mockService.savePatient(isA(Patient.class))).thenReturn(patient);
		returnedPatient = mockService.savePatient(new Patient());
		Assert.assertEquals("Returned object isn't the same as what should be returned!", patient, returnedPatient);

		// deletePatient test
		doNothing().when(mockService).deletePatient(Mockito.anyString());
		mockService.deletePatient("test-id");
		verify(mockService, times(1)).deletePatient("test-id");

		// deleteAllPatients test
		doNothing().when(mockService).deleteAllPatients();
		mockService.deleteAllPatients();
		verify(mockService, times(1)).deleteAllPatients();

		// countAllDoctors test
		when(mockService.countAllPatients()).thenReturn((Long) 5l);
		returnedCount = mockService.countAllPatients();
		Assert.assertEquals("Returned count isn't the same as what should be returned!", (Long) 5l, returnedCount);

		// findByFullName test
		when(mockService.findByFullName(Mockito.anyString())).thenReturn(patientsList);
		returnedList = mockService.findByFullName("Test");
		Assert.assertEquals("Returned list isn't the same as what should be returned!", patientsList, returnedList);

		// findAllLazyLoading test
		when(mockService.findAllLazyLoading(Mockito.anyInt(), Mockito.anyInt())).thenReturn(patientsList);
		returnedList = mockService.findAllLazyLoading(0, 1);
		Assert.assertEquals("Returned list isn't the same as what should be returned!", patientsList, returnedList);

		// sortAllLazyLoading test
		when(mockService.sortAllLazyLoading(Mockito.anyInt(), Mockito.anyInt(), isA(Sort.class)))
				.thenReturn(patientsList);
		returnedList = mockService.sortAllLazyLoading(0, 1, Sort.by("test").ascending());
		Assert.assertEquals("Returned list isn't the same as what should be returned!", patientsList, returnedList);

		// findByCnpNotId test
		when(mockService.findByCnpNotId(Mockito.anyString(), Mockito.anyString())).thenReturn(patient);
		returnedPatient = mockService.findByCnpNotId("1990101000000", "test-id");
		Assert.assertEquals("Returned object isn't the same as what should be returned!", patient, returnedPatient);

	}
}
