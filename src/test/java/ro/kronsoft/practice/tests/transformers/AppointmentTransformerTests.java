package ro.kronsoft.practice.tests.transformers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import ro.kronsoft.practice.dto.AppointmentDto;
import ro.kronsoft.practice.entities.Appointment;
import ro.kronsoft.practice.entities.enums.AppointmentStatus;
import ro.kronsoft.practice.entities.enums.AppointmentType;
import ro.kronsoft.practice.entities.enums.SpecialtyType;
import ro.kronsoft.practice.transformers.AppointmentTransformer;

@RunWith(PowerMockRunner.class)
@PrepareForTest(fullyQualifiedNames = "ro.kronsoft.practice.transformers.*")
class AppointmentTransformerTests {

	private AppointmentTransformer mockTransformer;

	@Test
	void testTransformerMethods() throws Exception {
		mockTransformer = Mockito.mock(AppointmentTransformer.class);

		Appointment appointment = new Appointment(AppointmentType.REGULAR, AppointmentStatus.CONFIRMED,
				SpecialtyType.CARDIOLOGY, LocalDate.parse("2020-11-04"), "10:00", "11:00", "Description", "testid",
				"testid");
		AppointmentDto dto = new AppointmentDto(AppointmentType.REGULAR, AppointmentStatus.CONFIRMED,
				SpecialtyType.CARDIOLOGY, LocalDate.parse("2020-11-04"), "10:00", "11:00", "Description", "testid",
				"testid");
		Appointment returnAppointment = new Appointment();
		AppointmentDto returnDto = new AppointmentDto();
		List<AppointmentDto> appointmentsList = new ArrayList<>();
		Iterable<AppointmentDto> returnedList = new ArrayList<>();

		// toDto test
		when(mockTransformer.toDto(Mockito.any(Appointment.class))).thenReturn(dto);
		returnDto = mockTransformer.toDto(appointment);
		assertEquals(appointment.getAppointmentType(), returnDto.getAppointmentType(),
				"The appointmentTypes aren't the same!");
		assertEquals(appointment.getAppointmentStatus(), returnDto.getAppointmentStatus(),
				"The appointmentStatus isn't the same!");
		assertEquals(appointment.getSpecialtyType(), returnDto.getSpecialtyType(), "The specialtyType isn't the same!");
		assertEquals(appointment.getDate(), returnDto.getDate(), "The dates aren't the same!");
		assertEquals(appointment.getStartTime(), returnDto.getStartTime(), "The startTimes aren't the same!");
		assertEquals(appointment.getEndTime(), returnDto.getEndTime(), "The endTimes aren't the same!");
		assertEquals(appointment.getDescription(), returnDto.getDescription(), "The descriptions aren't the same!");
		assertEquals(appointment.getDoctorId(), returnDto.getDoctorId(), "The id's aren't the same!");
		assertEquals(appointment.getPatientId(), returnDto.getPatientId(), "The id's aren't the same!");

		// toEntity test
		when(mockTransformer.toEntity(Mockito.any(AppointmentDto.class))).thenReturn(appointment);
		returnAppointment = mockTransformer.toEntity(dto);
		assertEquals(dto.getAppointmentType(), returnAppointment.getAppointmentType(),
				"The appointmentTypes aren't the same!");
		assertEquals(dto.getAppointmentStatus(), returnAppointment.getAppointmentStatus(),
				"The appointmentStatus isn't the same!");
		assertEquals(dto.getSpecialtyType(), returnAppointment.getSpecialtyType(), "The specialtyType isn't the same!");
		assertEquals(dto.getDate(), returnAppointment.getDate(), "The dates aren't the same!");
		assertEquals(dto.getStartTime(), returnAppointment.getStartTime(), "The startTimes aren't the same!");
		assertEquals(dto.getEndTime(), returnAppointment.getEndTime(), "The endTimes aren't the same!");
		assertEquals(dto.getDescription(), returnAppointment.getDescription(), "The descriptions aren't the same!");
		assertEquals(dto.getDoctorId(), returnAppointment.getDoctorId(), "The id's aren't the same!");
		assertEquals(dto.getPatientId(), returnAppointment.getPatientId(), "The id's aren't the same!");

		// toDtoList test
		when(mockTransformer.toDtoList(Mockito.anyListOf(Appointment.class))).thenReturn(appointmentsList);
		returnedList = mockTransformer.toDtoList(new ArrayList<Appointment>());
		assertEquals(appointmentsList, returnedList, "Returned list isn't the same as what should be returned!");
	}
}
