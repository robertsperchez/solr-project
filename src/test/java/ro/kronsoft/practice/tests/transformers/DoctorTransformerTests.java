package ro.kronsoft.practice.tests.transformers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import ro.kronsoft.practice.dto.DoctorDto;
import ro.kronsoft.practice.entities.Doctor;
import ro.kronsoft.practice.entities.enums.SpecialtyType;
import ro.kronsoft.practice.transformers.DoctorTransformer;

@RunWith(PowerMockRunner.class)
@PrepareForTest(fullyQualifiedNames = "ro.kronsoft.practice.transformers.*")
class DoctorTransformerTests {

	private DoctorTransformer mockTransformer;

	@Test
	void testTransformerMethods() throws Exception {
		mockTransformer = Mockito.mock(DoctorTransformer.class);

		Doctor doctor = new Doctor("Test", "Test", "test test", SpecialtyType.CARDIOLOGY, "0720000000",
				"test@test.com");
		DoctorDto dto = new DoctorDto("Test", "Test", "test test", SpecialtyType.CARDIOLOGY, "0720000000",
				"test@test.com");
		Doctor returnDoctor = new Doctor();
		DoctorDto returnDto = new DoctorDto();
		List<DoctorDto> doctorsList = new ArrayList<>();
		List<DoctorDto> returnedList = new ArrayList<>();

		// toDto test
		when(mockTransformer.toDto(Mockito.any(Doctor.class))).thenReturn(dto);
		returnDto = mockTransformer.toDto(doctor);
		assertEquals(doctor.getFirstName(), returnDto.getFirstName(), "The first names aren't the same!");
		assertEquals(doctor.getLastName(), returnDto.getLastName(), "The last names aren't the same!");
		assertEquals(doctor.getFullName(), returnDto.getFullName(), "The full names aren't the same!");
		assertEquals(doctor.getSpecialtyType(), returnDto.getSpecialtyType(), "The specialty types aren't the same!");
		assertEquals(doctor.getPhoneNumber(), returnDto.getPhoneNumber(), "The phone numbers aren't the same!");
		assertEquals(doctor.getEmail(), returnDto.getEmail(), "The emails aren't the same!");

		// toEntity test
		when(mockTransformer.toEntity(Mockito.any(DoctorDto.class))).thenReturn(doctor);
		returnDoctor = mockTransformer.toEntity(dto);
		assertEquals(dto.getFirstName(), returnDoctor.getFirstName(), "The first names aren't the same!");
		assertEquals(dto.getLastName(), returnDoctor.getLastName(), "The last names aren't the same!");
		assertEquals(dto.getFullName(), returnDoctor.getFullName(), "The full names aren't the same!");
		assertEquals(dto.getSpecialtyType(), returnDoctor.getSpecialtyType(), "The specialty types aren't the same!");
		assertEquals(dto.getPhoneNumber(), returnDoctor.getPhoneNumber(), "The phone numbers aren't the same!");
		assertEquals(dto.getEmail(), returnDoctor.getEmail(), "The emails aren't the same!");

		// toDtoList test
		when(mockTransformer.toDtoList(Mockito.anyListOf(Doctor.class))).thenReturn(doctorsList);
		returnedList = mockTransformer.toDtoList(new ArrayList<Doctor>());
		assertEquals(doctorsList, returnedList, "Returned list isn't the same as what should be returned!");
	}
}
