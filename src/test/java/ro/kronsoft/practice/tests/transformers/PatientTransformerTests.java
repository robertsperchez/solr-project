package ro.kronsoft.practice.tests.transformers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import ro.kronsoft.practice.dto.PatientDto;
import ro.kronsoft.practice.entities.Patient;
import ro.kronsoft.practice.entities.enums.SexTypeEnum;
import ro.kronsoft.practice.transformers.PatientTransformer;

@RunWith(PowerMockRunner.class)
@PrepareForTest(fullyQualifiedNames = "ro.kronsoft.practice.transformers.*")
class PatientTransformerTests {

	private PatientTransformer mockTransformer;

	@Test
	void testTransformerMethods() throws Exception {
		mockTransformer = Mockito.mock(PatientTransformer.class);

		Patient patient = new Patient("testid", "Test", "Test", "1990101000000", SexTypeEnum.MALE,
				LocalDate.parse("1999-01-01"), "0720000000", "Test", "Test");
		PatientDto dto = new PatientDto("Test", "Test", "test test", "1990101000000", SexTypeEnum.MALE,
				LocalDate.parse("1999-01-01"), "0720000000", "Test", "Test");
		Patient returnPatient = new Patient();
		PatientDto returnDto = new PatientDto();
		List<PatientDto> patientsList = new ArrayList<>();
		List<PatientDto> returnedList = new ArrayList<>();

		// toDto test
		when(mockTransformer.toDto(Mockito.any(Patient.class))).thenReturn(dto);
		returnDto = mockTransformer.toDto(patient);
		assertEquals(patient.getFirstName(), returnDto.getFirstName(), "The first names aren't the same!");
		assertEquals(patient.getLastName(), returnDto.getLastName(), "The last names aren't the same!");
		assertEquals(patient.getCnp(), returnDto.getCnp(), "The cnp's aren't the same!");
		assertEquals(patient.getSexType(), returnDto.getSexType(), "The sex types aren't the same!");
		assertEquals(patient.getBirthDate(), returnDto.getBirthDate(), "The birth dates aren't the same!");
		assertEquals(patient.getPhoneNumber(), returnDto.getPhoneNumber(), "The phone numbers aren't the same!");
		assertEquals(patient.getCity(), returnDto.getCity(), "The cities aren't the same!");
		assertEquals(patient.getCountry(), returnDto.getCountry(), "The countries aren't the same!");

		// toEntity test
		when(mockTransformer.toEntity(Mockito.any(PatientDto.class))).thenReturn(patient);
		returnPatient = mockTransformer.toEntity(dto);
		assertEquals(dto.getFirstName(), returnPatient.getFirstName(), "The first names aren't the same!");
		assertEquals(dto.getLastName(), returnPatient.getLastName(), "The last names aren't the same!");
		assertEquals(dto.getCnp(), returnPatient.getCnp(), "The cnp's aren't the same!");
		assertEquals(dto.getSexType(), returnPatient.getSexType(), "The sex types aren't the same!");
		assertEquals(dto.getBirthDate(), returnPatient.getBirthDate(), "The birth dates aren't the same!");
		assertEquals(dto.getPhoneNumber(), returnPatient.getPhoneNumber(), "The phone numbers aren't the same!");
		assertEquals(dto.getCity(), returnPatient.getCity(), "The cities aren't the same!");
		assertEquals(dto.getCountry(), returnPatient.getCountry(), "The countries aren't the same!");

		// toDtoList test
		when(mockTransformer.toDtoList(Mockito.anyListOf(Patient.class))).thenReturn(patientsList);
		returnedList = mockTransformer.toDtoList(new ArrayList<Patient>());
		assertEquals(patientsList, returnedList, "Returned list isn't the same as what should be returned!");
	}

}
