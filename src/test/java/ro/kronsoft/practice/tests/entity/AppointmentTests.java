package ro.kronsoft.practice.tests.entity;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import ro.kronsoft.practice.entities.Appointment;
import ro.kronsoft.practice.entities.enums.AppointmentStatus;
import ro.kronsoft.practice.entities.enums.AppointmentType;
import ro.kronsoft.practice.entities.enums.SpecialtyType;

class AppointmentTests {

	@Test
	void emptyConstructorTest() {
		Appointment appointment = new Appointment();
		assertEquals("This isn't an Appointment object!", Appointment.class, appointment.getClass());
		assertEquals("The id isn't null!", null, appointment.getId());
		assertEquals("The appointmentType isn't null!", null, appointment.getAppointmentType());
	}

	@Test
	void constructorTest() {
		Appointment appointment = new Appointment(AppointmentType.REGULAR, AppointmentStatus.CONFIRMED,
				SpecialtyType.CARDIOLOGY, LocalDate.parse("2020-11-04"), "10:00", "11:00", "Description", "testid",
				"testid");
		assertEquals("This isn't an Appointment object!", Appointment.class, appointment.getClass());
		assertEquals("The appointmentTypes aren't the same!", AppointmentType.REGULAR,
				appointment.getAppointmentType());
		assertEquals("The appointmentStatus isn't the same!", AppointmentStatus.CONFIRMED,
				appointment.getAppointmentStatus());
		assertEquals("The specialtyType isn't the same!", SpecialtyType.CARDIOLOGY, appointment.getSpecialtyType());
		assertEquals("The dates aren't the same!", LocalDate.parse("2020-11-04"), appointment.getDate());
		assertEquals("The startTimes aren't the same!", "10:00", appointment.getStartTime());
		assertEquals("The endTimes aren't the same!", "11:00", appointment.getEndTime());
		assertEquals("The descriptions aren't the same!", "Description", appointment.getDescription());
		assertEquals("The id's aren't the same!", "testid", appointment.getPatientId());
		assertEquals("The id's aren't the same!", "testid", appointment.getDoctorId());
	}

}
