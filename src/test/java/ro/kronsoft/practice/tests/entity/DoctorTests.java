package ro.kronsoft.practice.tests.entity;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import ro.kronsoft.practice.entities.Doctor;
import ro.kronsoft.practice.entities.enums.SpecialtyType;

class DoctorTests {

	@Test
	void emptyConstructorTest() {
		Doctor doctor = new Doctor();
		assertEquals("This isn't an Doctor object!", Doctor.class, doctor.getClass());
		assertEquals("The id isn't null!", null, doctor.getId());
		assertEquals("The first name isn't null!", null, doctor.getFirstName());
	}

	@Test
	void constructorTest() {
		Doctor doctor = new Doctor("Test", "Test", "test test", SpecialtyType.CARDIOLOGY, "0720000000",
				"test@test.com");
		assertEquals("This isn't an Doctor object!", Doctor.class, doctor.getClass());
		assertEquals("The first names aren't the same!", "Test", doctor.getFirstName());
		assertEquals("The last names aren't the same!", "Test", doctor.getLastName());
		assertEquals("The full names aren't the same!", "test test", doctor.getFullName());
		assertEquals("The specialty types aren't the same!", SpecialtyType.CARDIOLOGY, doctor.getSpecialtyType());
		assertEquals("The phone numbers aren't the same!", "0720000000", doctor.getPhoneNumber());
		assertEquals("The emails aren't the same!", "test@test.com", doctor.getEmail());
	}

}
