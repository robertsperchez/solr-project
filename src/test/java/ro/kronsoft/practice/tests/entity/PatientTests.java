package ro.kronsoft.practice.tests.entity;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import ro.kronsoft.practice.entities.Patient;
import ro.kronsoft.practice.entities.enums.SexTypeEnum;

class PatientTests {

	@Test
	void emptyConstructorTest() {
		Patient patient = new Patient();
		assertEquals("This isn't an Patient object!", Patient.class, patient.getClass());
		assertEquals("The id isn't null!", null, patient.getId());
		assertEquals("The first name isn't null!", null, patient.getFirstName());
	}

	@Test
	void constructorTest() {
		Patient patient = new Patient("testid", "Test", "Test", "1990101000000", SexTypeEnum.MALE,
				LocalDate.parse("1999-01-01"), "0720000000", "Test", "Test");
		assertEquals("This isn't an Patient object!", Patient.class, patient.getClass());
		assertEquals("The id's aren't the same!", "testid", patient.getId());
		assertEquals("The first names aren't the same!", "Test", patient.getFirstName());
		assertEquals("The last names aren't the same!", "Test", patient.getLastName());
		assertEquals("The cnp's aren't the same!", "1990101000000", patient.getCnp());
		assertEquals("The birth dates aren't the same!", LocalDate.parse("1999-01-01"), patient.getBirthDate());
		assertEquals("The phone numbers aren't the same!", "0720000000", patient.getPhoneNumber());
		assertEquals("The countries aren't the same!", "Test", patient.getCountry());
		assertEquals("The cities aren't the same!", "Test", patient.getCity());
		assertEquals("The sex types aren't the same!", SexTypeEnum.MALE, patient.getSexType());
	}
}
