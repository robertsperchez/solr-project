package ro.kronsoft.practice.tests;

import java.time.LocalDate;

import ro.kronsoft.practice.errors.BaseException;
import ro.kronsoft.practice.errors.NullParameterException;

public class Intersection {

	/**
	 * A method that checks the overlap between two intervals.
	 * 
	 * @param x1 Start of the first interval.
	 * @param y1 End of the first interval.
	 * @param x2 Start of the second interval.
	 * @param y2 End of the second interval.
	 * @return Returns true if there is an overlap, false otherwise.
	 */
	public static boolean checkInterval(final int x1, final int y1, final int x2, final int y2) {
		if (x1 > x2 && x1 < y2)
			return true;
		if (x2 > x1 && x2 < y1)
			return true;
		if (y1 > x2 && y1 < y2)
			return true;
		if (y2 > x1 && y2 < y1)
			return true;
		return false;
	}

	/**
	 * A method that checks if two date periods overlap.
	 * 
	 * @param xDateOne Start of the first period.
	 * @param yDateOne End of the first period.
	 * @param xDateTwo Start of the second period.
	 * @param yDateTwo End of the second period.
	 * @return Returns true if there is an overlap, false otherwise.
	 * @throws BaseException 
	 */
	public static boolean checkDatePeriods(LocalDate xDateOne, LocalDate yDateOne, LocalDate xDateTwo,
			LocalDate yDateTwo) throws BaseException {

//		xDateOne = Objects.requireNonNull(xDateOne, "xDateOne must not be null!");
//		yDateOne = Objects.requireNonNull(yDateOne, "yDateOne must not be null!");
//		xDateTwo = Objects.requireNonNull(xDateTwo, "xDateOne must not be null!");
//		yDateTwo = Objects.requireNonNull(yDateTwo, "xDateOne must not be null!");

		if (xDateOne == null) {
			throw new NullParameterException("xDateOne");
		}
		if (yDateOne == null) {
			throw new NullParameterException("yDateOne");
		}
		if (xDateTwo == null) {
			throw new NullParameterException("xDateTwo");
		}
		if (yDateTwo == null) {
			throw new NullParameterException("yDateTwo");
		}

		if (xDateOne.isAfter(xDateTwo) && xDateOne.isBefore(yDateTwo))
			return true;
		if (xDateTwo.isAfter(xDateOne) && xDateTwo.isBefore(yDateOne))
			return true;
		if (yDateOne.isAfter(xDateTwo) && yDateOne.isBefore(yDateTwo))
			return true;
		if (yDateTwo.isAfter(xDateOne) && yDateTwo.isBefore(yDateOne))
			return true;
		return false;
	}

}
