package ro.kronsoft.practice.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import ro.kronsoft.practice.errors.BaseException;

class IntersectionTests {

	/**
	 * A method to test the checkInterval method of the Intersection class.
	 */
	@Test
	void testIntersection() {
		// Case 1: no overlap
		assertFalse("There is an overlap!", Intersection.checkInterval(0, 10, 20, 30));
		// Case 2: no overlap, but the end of interval 1 is equals to the start of
		// interval 2
		assertFalse("There is an overlap!", Intersection.checkInterval(0, 10, 10, 30));
		// Case 3: overlap with the start of interval 2
		assertTrue("There isn't an overlap!", Intersection.checkInterval(10, 20, 15, 25));
		// Case 4: overlap with the end of interval 2
		assertTrue("There isn't an overlap!", Intersection.checkInterval(20, 30, 15, 25));
		// Case 5: whole first interval is in the second interval
		assertTrue("There isn't an overlap!", Intersection.checkInterval(10, 20, 8, 30));
		// Case 6: whole second interval is in the first interval
		assertTrue("There isn't an overlap!", Intersection.checkInterval(10, 30, 5, 25));
	}

	/**
	 * A method to test the checkDatePeriods method of the Intersection class.
	 * @throws BaseException 
	 */
	@Test
	void testDateOverlap() throws BaseException {
		// Case 1: no overlap
		assertFalse("There is an overlap!", Intersection.checkDatePeriods(LocalDate.parse("2020-01-01"),
				LocalDate.parse("2020-02-01"), LocalDate.parse("2020-03-01"), LocalDate.parse("2020-04-01")));
		// Case 2: no overlap, but the end of interval 1 is equals to the start of
		// interval 2
		assertFalse("There is an overlap!", Intersection.checkDatePeriods(LocalDate.parse("1999-01-01"),
				LocalDate.parse("1999-02-01"), LocalDate.parse("1999-02-01"), LocalDate.parse("1999-03-01")));
		// Case 3: overlap with the start of interval 2
		assertTrue("There isn't an overlap!", Intersection.checkDatePeriods(LocalDate.parse("1999-01-01"),
				LocalDate.parse("1999-06-01"), LocalDate.parse("1999-03-01"), LocalDate.parse("1999-09-01")));
		// Case 4: overlap with the end of interval 2
		assertTrue("There isn't an overlap!", Intersection.checkDatePeriods(LocalDate.parse("1999-06-01"),
				LocalDate.parse("1999-09-01"), LocalDate.parse("1999-04-01"), LocalDate.parse("1999-07-01")));
		// Case 5: whole first interval is in the second interval
		assertTrue("There isn't an overlap!", Intersection.checkDatePeriods(LocalDate.parse("1999-06-01"),
				LocalDate.parse("1999-07-01"), LocalDate.parse("1999-05-01"), LocalDate.parse("1999-09-01")));
		// Case 6: whole second interval is in the first interval
		assertTrue("There isn't an overlap!", Intersection.checkDatePeriods(LocalDate.parse("1999-01-01"),
				LocalDate.parse("1999-12-01"), LocalDate.parse("1999-06-01"), LocalDate.parse("1999-09-01")));
		assertTrue(Intersection.checkDatePeriods(null, null, null, null));
	}
}
