package ro.kronsoft.practice.tests.repository;

import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import ro.kronsoft.practice.entities.Appointment;
import ro.kronsoft.practice.repository.AppointmentRepository;

@RunWith(PowerMockRunner.class)
@PrepareForTest(fullyQualifiedNames = "ro.kronsoft.practice.repository.*")
class AppointmentRepositoryTests {

	private AppointmentRepository mockRepository;

	@Test
	void testRepositoryMethods() throws Exception {
		mockRepository = Mockito.mock(AppointmentRepository.class);

		List<Appointment> appointmentsList = new ArrayList<>();
		Iterable<Appointment> returnedList = new ArrayList<>();
		Appointment appointment = new Appointment();
		Appointment returnedAppointment = new Appointment();
		Long returnedCount;

		// findAll test
		when(mockRepository.findAll()).thenReturn(appointmentsList);
		returnedList = mockRepository.findAll();
		Assert.assertEquals("Returned list isn't the same as what should be returned!", appointmentsList, returnedList);

		// findById test
		when(mockRepository.findById(Mockito.anyString())).thenReturn(appointment);
		returnedAppointment = mockRepository.findById("test-id");
		Assert.assertEquals("Returned object isn't the same as what should be returned!", appointment,
				returnedAppointment);

		// findAllByPatientId test
		when(mockRepository.findAllByPatientId(Mockito.anyString(), isA(Pageable.class))).thenReturn(appointmentsList);
		returnedList = mockRepository.findAllByPatientId("test-id", PageRequest.of(0, 5));
		Assert.assertEquals("Returned list isn't the same as what should be returned!", appointmentsList, returnedList);

		// findAllByDoctorId test
		when(mockRepository.findAllByDoctorId(Mockito.anyString(), isA(Pageable.class))).thenReturn(appointmentsList);
		returnedList = mockRepository.findAllByDoctorId("test-id", PageRequest.of(0, 5));
		Assert.assertEquals("Returned list isn't the same as what should be returned!", appointmentsList, returnedList);

		// countByPatientId test
		when(mockRepository.countByPatientId(Mockito.anyString())).thenReturn((Long) 5l);
		returnedCount = mockRepository.countByPatientId("test-id");
		Assert.assertEquals("Returned count isn't the same as what should be returned!", (Long) 5l, returnedCount);

		// countByDoctorId test
		when(mockRepository.countByDoctorId(Mockito.anyString())).thenReturn((Long) 5l);
		returnedCount = mockRepository.countByDoctorId("test-id");
		Assert.assertEquals("Returned count isn't the same as what should be returned!", (Long) 5l, returnedCount);

		// findByEmailNotId test
		when(mockRepository.findByDateAndDoctorId(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(appointmentsList);
		returnedList = mockRepository.findByDateAndDoctorId("test-id", "test-id");
		Assert.assertEquals("Returned list isn't the same as what should be returned!", appointmentsList, returnedList);

		// deleteById test
		doNothing().when(mockRepository).deleteById(Mockito.anyString());
		mockRepository.deleteById("test-id");
		verify(mockRepository, times(1)).deleteById("test-id");

		// filterAppointments test
		Map<String, Object> filterFields = null;
		Map<String, String> sortFields = null;
		when(mockRepository.filterAppointments(Mockito.anyMap(), Mockito.anyMap(), isA(Pageable.class)))
				.thenReturn(appointmentsList);
		returnedList = mockRepository.filterAppointments(filterFields, sortFields, PageRequest.of(0, 5));
		Assert.assertEquals("Returned list isn't the same as what should be returned!", appointmentsList, returnedList);
	}

}
