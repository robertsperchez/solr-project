package ro.kronsoft.practice.tests.repository;

import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import ro.kronsoft.practice.entities.Patient;
import ro.kronsoft.practice.repository.PatientRepository;

@RunWith(PowerMockRunner.class)
@PrepareForTest(fullyQualifiedNames = "ro.kronsoft.practice.repository.*")
class PatientRepositoryTests {

	private PatientRepository mockRepository;

	@Test
	void testRepositoryMethods() throws Exception {
		mockRepository = Mockito.mock(PatientRepository.class);

		List<Patient> patientsList = new ArrayList<>();
		Iterable<Patient> returnedList = new ArrayList<>();
		Patient patient = new Patient();
		Patient returnedPatient = new Patient();

		// findAll test
		when(mockRepository.findAll()).thenReturn(patientsList);
		returnedList = mockRepository.findAll();
		Assert.assertEquals("Returned list isn't the same as what should be returned!", patientsList, returnedList);

		// findById test
		when(mockRepository.findById(Mockito.anyString())).thenReturn(patient);
		returnedPatient = mockRepository.findById("test-id");
		Assert.assertEquals("Returned object isn't the same as what should be returned!", patient, returnedPatient);

		// findByFullNameStartingWith test
		when(mockRepository.findByFullNameStartingWith(Mockito.anyString())).thenReturn(patientsList);
		returnedList = mockRepository.findByFullNameStartingWith("Test");
		Assert.assertEquals("Returned list isn't the same as what should be returned!", patientsList, returnedList);

		// findByCnp test
		when(mockRepository.findByCnp(Mockito.anyString())).thenReturn(patient);
		returnedPatient = mockRepository.findByCnp("1990101000000");
		Assert.assertEquals("Returned object isn't the same as what should be returned!", patient, returnedPatient);

		// findByCnpNotId test
		when(mockRepository.findByCnpNotId(Mockito.anyString(), Mockito.anyString())).thenReturn(patient);
		returnedPatient = mockRepository.findByCnpNotId("1990101000000", "test-id");
		Assert.assertEquals("Returned object isn't the same as what should be returned!", patient, returnedPatient);

		// deleteById test
		doNothing().when(mockRepository).deleteById(Mockito.anyString());
		mockRepository.deleteById("test-id");
		verify(mockRepository, times(1)).deleteById("test-id");

		// filterDoctors test
		Map<String, Object> filterFields = null;
		Map<String, String> sortFields = null;
		when(mockRepository.filterAppointments(Mockito.anyMap(), Mockito.anyMap(), isA(Pageable.class)))
				.thenReturn(patientsList);
		returnedList = mockRepository.filterPatients(filterFields, sortFields, PageRequest.of(0, 5));
		Assert.assertEquals("Returned list isn't the same as what should be returned!", patientsList, returnedList);
	}

}
