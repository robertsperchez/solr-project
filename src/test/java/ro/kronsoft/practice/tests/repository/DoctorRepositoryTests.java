package ro.kronsoft.practice.tests.repository;

import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import ro.kronsoft.practice.entities.Doctor;
import ro.kronsoft.practice.entities.enums.SpecialtyType;
import ro.kronsoft.practice.repository.DoctorRepository;

@RunWith(PowerMockRunner.class)
@PrepareForTest(fullyQualifiedNames = "ro.kronsoft.practice.repository.*")
class DoctorRepositoryTests {

	private DoctorRepository mockRepository;

	@Test
	void testRepositoryMethods() throws Exception {
		mockRepository = Mockito.mock(DoctorRepository.class);

		List<Doctor> doctorsList = new ArrayList<>();
		Iterable<Doctor> returnedList = new ArrayList<>();
		Doctor doctor = new Doctor();
		Doctor returnedDoctor = new Doctor();

		// findAll test
		when(mockRepository.findAll()).thenReturn(doctorsList);
		returnedList = mockRepository.findAll();
		Assert.assertEquals("Returned list isn't the same as what should be returned!", doctorsList, returnedList);

		// findByFullNameAndSpecialty test
		when(mockRepository.findByFullNameAndSpecialty(Mockito.anyString(), Mockito.anyObject()))
				.thenReturn(doctorsList);
		returnedList = mockRepository.findByFullNameAndSpecialty("Test", SpecialtyType.CARDIOLOGY);
		Assert.assertEquals("Returned list isn't the same as what should be returned!", doctorsList, returnedList);

		// findByFullNameStartingWith test
		when(mockRepository.findByFullNameStartingWith(Mockito.anyString())).thenReturn(doctorsList);
		returnedList = mockRepository.findByFullNameStartingWith("Test");
		Assert.assertEquals("Returned list isn't the same as what should be returned!", doctorsList, returnedList);

		// findById test
		when(mockRepository.findById(Mockito.anyString())).thenReturn(doctor);
		returnedDoctor = mockRepository.findById("test-id");
		Assert.assertEquals("Returned object isn't the same as what should be returned!", doctor, returnedDoctor);

		// findByEmail test
		when(mockRepository.findByEmail(Mockito.anyString())).thenReturn(doctor);
		returnedDoctor = mockRepository.findByEmail("test-id");
		Assert.assertEquals("Returned object isn't the same as what should be returned!", doctor, returnedDoctor);

		// findByEmailNotId test
		when(mockRepository.findByEmailNotId(Mockito.anyString(), Mockito.anyString())).thenReturn(doctor);
		returnedDoctor = mockRepository.findByEmailNotId("test-id", "test-id");
		Assert.assertEquals("Returned object isn't the same as what should be returned!", doctor, returnedDoctor);

		// findByFullName test
		when(mockRepository.findByFullName(Mockito.anyString())).thenReturn(doctor);
		returnedDoctor = mockRepository.findByFullName("test-id");
		Assert.assertEquals("Returned object isn't the same as what should be returned!", doctor, returnedDoctor);

		// deleteById test
		doNothing().when(mockRepository).deleteById(Mockito.anyString());
		mockRepository.deleteById("test-id");
		verify(mockRepository, times(1)).deleteById("test-id");

		// filterDoctors test
		Map<String, Object> filterFields = null;
		Map<String, String> sortFields = null;
		when(mockRepository.filterDoctors(Mockito.anyMap(), Mockito.anyMap(), isA(Pageable.class)))
				.thenReturn(doctorsList);
		returnedList = mockRepository.filterDoctors(filterFields, sortFields, PageRequest.of(0, 5));
		Assert.assertEquals("Returned list isn't the same as what should be returned!", doctorsList, returnedList);
	}

}
