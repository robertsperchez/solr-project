package ro.kronsoft.practice.tests.controllers;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import ro.kronsoft.practice.controllers.DoctorController;

@RunWith(PowerMockRunner.class)
@PrepareForTest(fullyQualifiedNames = "ro.kronsoft.practice.controllers.*")
class DoctorControllerTests {

	private DoctorController mockController;

	@Test
	void testControllerMethods() throws Exception {
		mockController = Mockito.mock(DoctorController.class);

		// deleteDoctor test
		doNothing().when(mockController).deleteDoctor(Mockito.anyString());
		mockController.deleteDoctor("test-id");
		verify(mockController, times(1)).deleteDoctor("test-id");
	}

}
