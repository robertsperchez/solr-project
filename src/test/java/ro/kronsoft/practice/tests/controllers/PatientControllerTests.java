package ro.kronsoft.practice.tests.controllers;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import ro.kronsoft.practice.controllers.PatientController;

@RunWith(PowerMockRunner.class)
@PrepareForTest(fullyQualifiedNames = "ro.kronsoft.practice.controllers.*")
class PatientControllerTests {

	private PatientController mockController;

	@Test
	void testControllerMethods() throws Exception {
		mockController = Mockito.mock(PatientController.class);

		// deletePatient test
		doNothing().when(mockController).deletePatient(Mockito.anyString());
		mockController.deletePatient("test-id");
		verify(mockController, times(1)).deletePatient("test-id");

	}

}
