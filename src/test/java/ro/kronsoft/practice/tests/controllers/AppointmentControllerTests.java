package ro.kronsoft.practice.tests.controllers;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import ro.kronsoft.practice.controllers.AppointmentController;
import ro.kronsoft.practice.dto.DoctorDto;
import ro.kronsoft.practice.dto.PatientDto;

@RunWith(PowerMockRunner.class)
@PrepareForTest(fullyQualifiedNames = "ro.kronsoft.practice.controllers.*")
class AppointmentControllerTests {

	private AppointmentController mockController;

	@Test
	void testControllerMethods() throws Exception {
		mockController = Mockito.mock(AppointmentController.class);

		List<PatientDto> patientsList = new ArrayList<>();
		Iterable<PatientDto> returnedPatientsList = new ArrayList<>();
		List<DoctorDto> doctorsList = new ArrayList<>();
		Iterable<DoctorDto> returnedDoctorsList = new ArrayList<>();

		// completePatient test
		when(mockController.completePatient(Mockito.anyString())).thenReturn(patientsList);
		returnedPatientsList = mockController.completePatient("test");
		Assert.assertEquals("Returned list isn't the same as what should be returned!", patientsList,
				returnedPatientsList);

		// setSpecialtyForAutocomplete test
		doNothing().when(mockController).setSpecialtyForAutocomplete();
		mockController.setSpecialtyForAutocomplete();
		verify(mockController, times(1)).setSpecialtyForAutocomplete();

		// completeDoctor test
		when(mockController.completeDoctor(Mockito.anyString())).thenReturn(doctorsList);
		returnedDoctorsList = mockController.completeDoctor("test");
		Assert.assertEquals("Returned list isn't the same as what should be returned!", doctorsList,
				returnedDoctorsList);

		// completeDoctorFilter test
		when(mockController.completeDoctorFilter(Mockito.anyString())).thenReturn(doctorsList);
		returnedDoctorsList = mockController.completeDoctorFilter("test");
		Assert.assertEquals("Returned list isn't the same as what should be returned!", doctorsList,
				returnedDoctorsList);

		// deleteAppointment test
		doNothing().when(mockController).deleteAppointment(Mockito.anyString());
		mockController.deleteAppointment("test-id");
		verify(mockController, times(1)).deleteAppointment("test-id");
	}
}
