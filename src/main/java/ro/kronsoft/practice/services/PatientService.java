package ro.kronsoft.practice.services;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import ro.kronsoft.practice.entities.Patient;
import ro.kronsoft.practice.repository.PatientRepository;

@Service("patientService")
public class PatientService {

	@Autowired
	private PatientRepository patientRepository;

	public Patient getPatientById(final String id) {
		return patientRepository.findById(id);
	}

	public Patient findPatientByCnp(final String cnp) {
		return patientRepository.findByCnp(cnp);
	}

	public Iterable<Patient> getAllPatients() {
		return patientRepository.findAll();
	}

	public Patient savePatient(Patient patient) {
		if (patient.getId() == null)
			patient.setId(UUID.randomUUID().toString());
		patient.setFullName(patient.getFirstName().toLowerCase() + " " + patient.getLastName().toLowerCase());
		return patientRepository.save(patient);
	}

	public void deletePatient(final String id) {
		patientRepository.deleteById(id);
	}

	public void deleteAllPatients() {
		patientRepository.deleteAll();
	}

	public Long countAllPatients() {
		return patientRepository.count();
	}

	public List<Patient> findByFullName(final String query) {
		return patientRepository.findByFullNameStartingWith(query);
	}

	public List<Patient> findAllLazyLoading(final int pageNumber, final int pageSize) {
		Pageable results = PageRequest.of(pageNumber, pageSize);
		return patientRepository.findAll(results).getContent();
	}

	public List<Patient> sortAllLazyLoading(final int pageNumber, final int pageSize, final Sort sort) {
		Pageable results = PageRequest.of(pageNumber, pageSize, sort);
		return patientRepository.findAll(results).getContent();
	}

	public List<Patient> filterPatients(final Map<String, Object> filterFields, final Map<String, String> sortFields,
			final Pageable page) {
		return patientRepository.filterPatients(filterFields, sortFields, page);
	}

	public Patient findByCnpNotId(final String cnp, final String id) {
		return patientRepository.findByCnpNotId(cnp, id);
	}
}
