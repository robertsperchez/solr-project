package ro.kronsoft.practice.services;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import ro.kronsoft.practice.entities.Appointment;
import ro.kronsoft.practice.repository.AppointmentRepository;

@Service("appointmentService")
public class AppointmentService {

	@Autowired
	private AppointmentRepository appointmentRepository;

	public Appointment getAppointmentById(final String id) {
		return appointmentRepository.findById(id);
	}

	public Iterable<Appointment> getAllAppointments() {
		return appointmentRepository.findAll();
	}

	public Appointment saveAppointment(Appointment appointment) {
		if (appointment.getId() == null)
			appointment.setId(UUID.randomUUID().toString());
		return appointmentRepository.save(appointment);
	}

	public void deleteAppointment(final String id) {
		appointmentRepository.deleteById(id);
	}

	public void deleteAllAppointments() {
		appointmentRepository.deleteAll();
	}

	public void deleteAllByPatientId(String patientId) {
		appointmentRepository.deleteAllByPatientId(patientId);
	}

	public void deleteAllByDoctorId(String doctorId) {
		appointmentRepository.deleteAllByDoctorId(doctorId);
	}

	public Long countAllAppointments() {
		return appointmentRepository.count();
	}

	public Long countByPatientId(final String patientId) {
		return appointmentRepository.countByPatientId(patientId);
	}

	public Long countByDoctorId(final String doctorId) {
		return appointmentRepository.countByDoctorId(doctorId);
	}

	public List<Appointment> findAllLazyLoading(final int pageNumber, final int pageSize) {
		Pageable results = PageRequest.of(pageNumber, pageSize);
		return appointmentRepository.findAll(results).getContent();
	}

	public List<Appointment> sortAllLazyLoading(final int pageNumber, final int pageSize, Sort sort) {
		Pageable results = PageRequest.of(pageNumber, pageSize, sort);
		return appointmentRepository.findAll(results).getContent();
	}

	public List<Appointment> findAllByPatientId(final String patientId, final int pageNumber, final int pageSize) {
		Pageable results = PageRequest.of(pageNumber, pageSize);
		return appointmentRepository.findAllByPatientId(patientId, results);
	}

	public List<Appointment> findAllByPatientId(final String patientId, final int pageNumber, final int pageSize,
			final Sort sort) {
		Pageable results = PageRequest.of(pageNumber, pageSize, sort);
		return appointmentRepository.findAllByPatientId(patientId, results);
	}

	public List<Appointment> findAllByDoctorId(final String doctorId, final int pageNumber, final int pageSize) {
		Pageable results = PageRequest.of(pageNumber, pageSize);
		return appointmentRepository.findAllByPatientId(doctorId, results);
	}

	public List<Appointment> findAllByDoctorId(final String doctorId, final int pageNumber, final int pageSize,
			final Sort sort) {
		Pageable results = PageRequest.of(pageNumber, pageSize, sort);
		return appointmentRepository.findAllByPatientId(doctorId, results);
	}

	public List<Appointment> filterAppointments(final Map<String, Object> filterFields,
			final Map<String, String> sortFields, final Pageable page) {
		return appointmentRepository.filterAppointments(filterFields, sortFields, page);
	}

	public List<Appointment> findByDateAndDoctorId(final String date, final String doctorId) {
		return appointmentRepository.findByDateAndDoctorId(date, doctorId);
	}
}
