package ro.kronsoft.practice.services;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import ro.kronsoft.practice.entities.Doctor;
import ro.kronsoft.practice.entities.enums.SpecialtyType;
import ro.kronsoft.practice.repository.DoctorRepository;

@Service("doctorService")
public class DoctorService {

	@Autowired
	private DoctorRepository doctorRepository;

	public Doctor findDoctorById(final String id) {
		return doctorRepository.findById(id);
	}

	public Doctor findDoctorByEmail(final String email) {
		return doctorRepository.findByEmail(email);
	}

	public Iterable<Doctor> getAllDoctors() {
		return doctorRepository.findAll();
	}

	public Doctor saveDoctor(Doctor doctor) {
		if (doctor.getId() == null)
			doctor.setId(UUID.randomUUID().toString());
		doctor.setFullName(doctor.getFirstName().toLowerCase() + " " + doctor.getLastName().toLowerCase());
		return doctorRepository.save(doctor);
	}

	public void deleteDoctor(final String id) {
		doctorRepository.deleteById(id);
	}

	public void deleteAllDoctors() {
		doctorRepository.deleteAll();
	}

	public Long countAllDoctors() {
		return doctorRepository.count();
	}

	public List<Doctor> findByFullNameAndSpecialty(final String query, final SpecialtyType specialtyType) {
		return doctorRepository.findByFullNameAndSpecialty(query, specialtyType);
	}

	public List<Doctor> findByFullNameStartingWith(final String query) {
		return doctorRepository.findByFullNameStartingWith(query);
	}

	public List<Doctor> findAllLazyLoading(final int pageNumber, final int pageSize) {
		Pageable results = PageRequest.of(pageNumber, pageSize);
		return doctorRepository.findAll(results).getContent();
	}

	public List<Doctor> sortAllLazyLoading(final int pageNumber, final int pageSize, final Sort sort) {
		Pageable results = PageRequest.of(pageNumber, pageSize, sort);
		return doctorRepository.findAll(results).getContent();
	}

	public List<Doctor> filterDoctors(final Map<String, Object> filterFields, final Map<String, String> sortFields,
			final Pageable page) {
		return doctorRepository.filterDoctors(filterFields, sortFields, page);
	}

	public Doctor findByEmailNotId(final String email, final String id) {
		return doctorRepository.findByEmailNotId(email, id);
	}

	public Doctor findByFullName(final String fullName) {
		return doctorRepository.findByFullName(fullName.toLowerCase());
	}
}
