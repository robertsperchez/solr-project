package ro.kronsoft.practice.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.PrimeFaces;
import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;
import org.springframework.data.domain.PageRequest;

import ro.kronsoft.practice.dto.AppointmentDto;
import ro.kronsoft.practice.dto.DoctorDto;
import ro.kronsoft.practice.entities.Appointment;
import ro.kronsoft.practice.entities.Doctor;
import ro.kronsoft.practice.services.AppointmentService;
import ro.kronsoft.practice.transformers.AppointmentTransformer;
import ro.kronsoft.practice.transformers.DoctorTransformer;

@ManagedBean(name = "lazyPatientAppointmentsModel")
@ViewScoped
public class LazyPatientAppointmentsModel extends LazyDataModel<AppointmentDto> {

	private static final long serialVersionUID = 3555755473128504488L;

	@ManagedProperty(value = "#{appointmentService}")
	private transient AppointmentService appointmentService;

	@ManagedProperty(value = "#{appointmentTransformer}")
	private AppointmentTransformer appointmentTransformer;

	@ManagedProperty(value = "#{doctorTransformer}")
	private DoctorTransformer doctorTransformer;

	private Map<String, Object> filterFields;

	private Map<String, String> sortFields;

	private Long appointmentsCount;

	private String patientId;

	public LazyPatientAppointmentsModel() {
		filterFields = new HashMap<>();
		sortFields = new HashMap<>();
	}

	@Override
	public List<AppointmentDto> load(int first, int pageSize, Map<String, SortMeta> sortMeta,
			Map<String, FilterMeta> filterMeta) {

		List<Appointment> data = new ArrayList<>();

		for (FilterMeta meta : filterMeta.values()) {
			String filterField = meta.getFilterField();
			Object filterValue = meta.getFilterValue();
			if (filterValue != null && filterValue.toString().length() > 1000) {
				filterValue = null;
				PrimeFaces current = PrimeFaces.current();
				current.executeScript("PF('filterErrorDialog').show();");
			}
			if (filterValue instanceof DoctorDto) {
				filterValue = doctorTransformer.toEntity((DoctorDto) filterValue);
			}
			filterDataTable(filterField, filterValue);
		}
		for (SortMeta meta : sortMeta.values()) {
			String sortField = meta.getSortField();
			String sortOrderString = meta.getSortOrder().toString();
			sortDataTable(sortField, sortOrderString);
		}

		filterFields.put(LazyAppointmentModel.constPatientId, patientId);
		if (patientId != null && !filterFields.isEmpty()) {
			data = appointmentService.filterAppointments(filterFields, sortFields,
					PageRequest.of(first / pageSize, pageSize));
			this.setRowCount(data.size());
		}
		return appointmentTransformer.toDtoList(data);
	}

	/**
	 * Fills a map object with the selected sort fields and order.
	 * 
	 * @param sortKey   The sort field.
	 * @param sortOrder
	 */
	public void sortDataTable(final String sortKey, final String sortOrder) {
		switch (sortKey) {
		case LazyAppointmentModel.constAppointmentType:
			if (sortOrder != null) {
				sortFields.put(LazyAppointmentModel.constAppointmentType, sortOrder);
			} else {
				sortFields.remove(LazyAppointmentModel.constAppointmentType);
			}
			break;
		case LazyAppointmentModel.constAppointmentStatus:
			if (sortOrder != null) {
				sortFields.put(LazyAppointmentModel.constAppointmentStatus, sortOrder);
			} else {
				sortFields.remove(LazyAppointmentModel.constAppointmentStatus);
			}
			break;
		case LazyAppointmentModel.constSpecialtyType:
			if (sortOrder != null) {
				sortFields.put(LazyAppointmentModel.constSpecialtyType, sortOrder);
			} else {
				sortFields.remove(LazyAppointmentModel.constSpecialtyType);
			}
			break;
		case LazyAppointmentModel.constDate:
			if (sortOrder != null) {
				sortFields.put(LazyAppointmentModel.constDate, sortOrder);
			} else {
				sortFields.remove(LazyAppointmentModel.constDate);
			}
			break;
		default:
			break;
		}
	}

	/**
	 * Puts the selected filter fields and values into a map object.
	 * 
	 * @param filterField
	 * @param filterValue
	 */
	public void filterDataTable(final String filterField, final Object filterValue) {
		switch (filterField) {
		case LazyAppointmentModel.constAppointmentType:
			if (filterValue != null) {
				filterFields.put(LazyAppointmentModel.constAppointmentType, filterValue.toString());
			} else {
				filterFields.remove(LazyAppointmentModel.constAppointmentType);
			}
			break;
		case LazyAppointmentModel.constAppointmentStatus:
			if (filterValue != null) {
				filterFields.put(LazyAppointmentModel.constAppointmentStatus, filterValue.toString());
			} else {
				filterFields.remove(LazyAppointmentModel.constAppointmentStatus);
			}
			break;
		case LazyAppointmentModel.constSpecialtyType:
			if (filterValue != null) {
				filterFields.put(LazyAppointmentModel.constSpecialtyType, filterValue.toString());
			} else {
				filterFields.remove(LazyAppointmentModel.constSpecialtyType);
			}
			break;
		case LazyAppointmentModel.constDate:
			if (filterValue != null) {
				filterFields.put(LazyAppointmentModel.constDate, filterValue);
			} else {
				filterFields.remove(LazyAppointmentModel.constDate);
			}
			break;
		case LazyAppointmentModel.constStartTime:
			if (filterValue != null) {
				filterFields.put(LazyAppointmentModel.constStartTime, filterValue.toString());
			} else {
				filterFields.remove(LazyAppointmentModel.constStartTime);
			}
			break;
		case LazyAppointmentModel.constEndTime:
			if (filterValue != null) {
				filterFields.put(LazyAppointmentModel.constEndTime, filterValue.toString());
			} else {
				filterFields.remove(LazyAppointmentModel.constEndTime);
			}
			break;
		case LazyAppointmentModel.constDoctorId:
			if (filterValue != null) {
				if (filterValue instanceof Doctor)
					filterFields.put(LazyAppointmentModel.constDoctorId, ((Doctor) filterValue).getId());
				else
					filterFields.put(LazyAppointmentModel.constDoctorId, filterValue.toString());
			} else {
				filterFields.remove(LazyAppointmentModel.constDoctorId);
			}
			break;
		default:
			break;
		}
	}

	public AppointmentService getAppointmentService() {
		return appointmentService;
	}

	public void setAppointmentService(AppointmentService appointmentService) {
		this.appointmentService = appointmentService;
	}

	public Map<String, Object> getFilterFields() {
		return filterFields;
	}

	public void setFilterFields(Map<String, Object> filterFields) {
		this.filterFields = filterFields;
	}

	public Map<String, String> getSortFields() {
		return sortFields;
	}

	public void setSortFields(Map<String, String> sortFields) {
		this.sortFields = sortFields;
	}

	public Long getAppointmentsCount() {
		return appointmentsCount;
	}

	public void setAppointmentsCount(Long appointmentsCount) {
		this.appointmentsCount = appointmentsCount;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public AppointmentTransformer getAppointmentTransformer() {
		return appointmentTransformer;
	}

	public void setAppointmentTransformer(AppointmentTransformer appointmentTransformer) {
		this.appointmentTransformer = appointmentTransformer;
	}

	public DoctorTransformer getDoctorTransformer() {
		return doctorTransformer;
	}

	public void setDoctorTransformer(DoctorTransformer doctorTransformer) {
		this.doctorTransformer = doctorTransformer;
	}
}