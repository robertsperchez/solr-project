package ro.kronsoft.practice.models;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import ro.kronsoft.practice.dto.PatientDto;
import ro.kronsoft.practice.entities.Appointment;
import ro.kronsoft.practice.entities.Patient;
import ro.kronsoft.practice.entities.enums.SexTypeEnum;

@ManagedBean(name = "patientModel")
@ViewScoped
public class PatientModel implements Serializable {

	private static final long serialVersionUID = 272395823092520221L;

	private List<Patient> patientList;

	private List<Appointment> patientAppointmentList;

	private List<SexTypeEnum> sexTypeList;

	private PatientDto patient;

	private LocalDate maxDate;

	public PatientModel() {
		super();
	}

	@PostConstruct
	public void init() {
		sexTypeList = Arrays.asList(SexTypeEnum.values());
		patientList = new ArrayList<>();
		patient = new PatientDto();
		maxDate = LocalDate.now();
	}

	public PatientDto getPatient() {
		return patient;
	}

	public void setPatient(PatientDto patient) {
		this.patient = patient;
	}

	public LocalDate getMaxDate() {
		return maxDate;
	}

	public void setMaxDate(LocalDate maxDate) {
		this.maxDate = maxDate;
	}

	public List<Appointment> getPatientAppointmentList() {
		return patientAppointmentList;
	}

	public void setPatientAppointmentList(List<Appointment> patientAppointmentList) {
		this.patientAppointmentList = patientAppointmentList;
	}

	public List<SexTypeEnum> getSexTypeList() {
		return sexTypeList;
	}

	public void setSexTypeList(List<SexTypeEnum> sexTypeList) {
		this.sexTypeList = sexTypeList;
	}

	public List<Patient> getPatientList() {
		return patientList;
	}

	public void setPatientList(List<Patient> patientList) {
		this.patientList = patientList;
	}
}
