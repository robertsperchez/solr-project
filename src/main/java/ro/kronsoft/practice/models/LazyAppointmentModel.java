package ro.kronsoft.practice.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.PrimeFaces;
import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import ro.kronsoft.practice.dto.AppointmentDto;
import ro.kronsoft.practice.dto.DoctorDto;
import ro.kronsoft.practice.dto.PatientDto;
import ro.kronsoft.practice.entities.Appointment;
import ro.kronsoft.practice.entities.Doctor;
import ro.kronsoft.practice.entities.Patient;
import ro.kronsoft.practice.services.AppointmentService;
import ro.kronsoft.practice.transformers.AppointmentTransformer;
import ro.kronsoft.practice.transformers.DoctorTransformer;
import ro.kronsoft.practice.transformers.PatientTransformer;

@ManagedBean(name = "lazyAppointmentModel")
@ViewScoped
public class LazyAppointmentModel extends LazyDataModel<AppointmentDto> {

	private static final long serialVersionUID = 3555755473128504488L;

	static final String constDate = "date";
	static final String constAppointmentType = "appointmentType";
	static final String constAppointmentStatus = "appointmentStatus";
	static final String constSpecialtyType = "specialtyType";
	static final String constStartTime = "startTime";
	static final String constEndTime = "endTime";
	static final String constPatientId = "patientId";
	static final String constDoctorId = "doctorId";

	@ManagedProperty(value = "#{appointmentService}")
	private transient AppointmentService appointmentService;

	@ManagedProperty(value = "#{appointmentTransformer}")
	private AppointmentTransformer appointmentTransformer;

	@ManagedProperty(value = "#{patientTransformer}")
	private PatientTransformer patientTransformer;

	@ManagedProperty(value = "#{doctorTransformer}")
	private DoctorTransformer doctorTransformer;

	private List<AppointmentDto> lazyAppointmentList;

	private Map<String, Object> filterFields;

	private Map<String, String> sortFields;

	private Long appointmentsCount;

	public LazyAppointmentModel() {
		filterFields = new HashMap<>();
		sortFields = new HashMap<>();
	}

	@Override
	public List<AppointmentDto> load(int first, int pageSize, Map<String, SortMeta> sortMeta,
			Map<String, FilterMeta> filterMeta) {

		List<Appointment> data = new ArrayList<>();
		String sortKey = new String();
		String sortOrder = new String();

		for (String k : sortMeta.keySet()) {
			sortKey = k;
		}
		for (SortMeta s : sortMeta.values()) {
			sortOrder = s.getSortOrder().toString();
		}

		for (FilterMeta meta : filterMeta.values()) {
			String filterField = meta.getFilterField();
			Object filterValue = meta.getFilterValue();
			if (filterValue != null && filterValue.toString().length() > 1000) {
				filterValue = null;
				PrimeFaces current = PrimeFaces.current();
				current.executeScript("PF('filterErrorDialog').show();");
			}
			if (filterValue instanceof PatientDto) {
				filterValue = patientTransformer.toEntity((PatientDto) filterValue);
			}
			if (filterValue instanceof DoctorDto) {
				filterValue = doctorTransformer.toEntity((DoctorDto) filterValue);
			}
			filterDataTable(filterField, filterValue);
		}
		for (SortMeta meta : sortMeta.values()) {
			String sortField = meta.getSortField();
			String sortOrderString = meta.getSortOrder().toString();
			sortDataTable(sortField, sortOrderString);
		}

		if (!filterFields.isEmpty()) {
			data = appointmentService.filterAppointments(filterFields, sortFields,
					PageRequest.of(first / pageSize, pageSize));
			this.setRowCount(data.size());
		} else if (sortKey.isEmpty() && filterFields.isEmpty()) {
			data = appointmentService.findAllLazyLoading(first / pageSize, pageSize);
			this.setRowCount(Math.toIntExact(this.getAppointmentsCount()));
		} else if (sortOrder.equals("ASCENDING")) {
			data = appointmentService.sortAllLazyLoading(first / pageSize, pageSize, Sort.by(sortKey).ascending());
			this.setRowCount(Math.toIntExact(this.getAppointmentsCount()));
		} else if (sortOrder.equals("DESCENDING")) {
			data = appointmentService.sortAllLazyLoading(first / pageSize, pageSize, Sort.by(sortKey).descending());
			this.setRowCount(Math.toIntExact(this.getAppointmentsCount()));
		}

		return appointmentTransformer.toDtoList(data);
	}

	/**
	 * Fills a map object with the selected sort fields and order.
	 * 
	 * @param sortKey   The sort field.
	 * @param sortOrder
	 */
	public void sortDataTable(final String sortKey, final String sortOrder) {
		switch (sortKey) {
		case constAppointmentType:
			if (sortOrder != null) {
				sortFields.put(constAppointmentType, sortOrder);
			} else {
				sortFields.remove(constAppointmentType);
			}
			break;
		case constAppointmentStatus:
			if (sortOrder != null) {
				sortFields.put(constAppointmentStatus, sortOrder);
			} else {
				sortFields.remove(constAppointmentStatus);
			}
			break;
		case constSpecialtyType:
			if (sortOrder != null) {
				sortFields.put(constSpecialtyType, sortOrder);
			} else {
				sortFields.remove(constSpecialtyType);
			}
			break;
		case constDate:
			if (sortOrder != null) {
				sortFields.put(constDate, sortOrder);
			} else {
				sortFields.remove(constDate);
			}
			break;
		default:
			break;
		}
	}

	/**
	 * Puts the selected filter fields and values into a map object.
	 * 
	 * @param filterField
	 * @param filterValue
	 */
	public void filterDataTable(final String filterField, final Object filterValue) {
		switch (filterField) {
		case constAppointmentType:
			if (filterValue != null) {
				filterFields.put(constAppointmentType, filterValue.toString());
			} else {
				filterFields.remove(constAppointmentType);
			}
			break;
		case constAppointmentStatus:
			if (filterValue != null) {
				filterFields.put(constAppointmentStatus, filterValue.toString());
			} else {
				filterFields.remove(constAppointmentStatus);
			}
			break;
		case constSpecialtyType:
			if (filterValue != null) {
				filterFields.put(constSpecialtyType, filterValue.toString());
			} else {
				filterFields.remove(constSpecialtyType);
			}
			break;
		case constDate:
			if (filterValue != null) {
				filterFields.put(constDate, filterValue);
			} else {
				filterFields.remove(constDate);
			}
			break;
		case constStartTime:
			if (filterValue != null) {
				filterFields.put(constStartTime, filterValue.toString());
			} else {
				filterFields.remove(constStartTime);
			}
			break;
		case constEndTime:
			if (filterValue != null) {
				filterFields.put(constEndTime, filterValue.toString());
			} else {
				filterFields.remove(constEndTime);
			}
			break;
		case constPatientId:
			if (filterValue != null) {
				if (filterValue instanceof Patient)
					filterFields.put(constPatientId, ((Patient) filterValue).getId());
				else
					filterFields.put(constPatientId, filterValue.toString());
			} else {
				filterFields.remove(constPatientId);
			}
			break;
		case constDoctorId:
			if (filterValue != null) {
				if (filterValue instanceof Doctor)
					filterFields.put(constDoctorId, ((Doctor) filterValue).getId());
				else
					filterFields.put(constDoctorId, filterValue.toString());
			} else {
				filterFields.remove(constDoctorId);
			}
			break;
		default:
			break;
		}
	}

	public AppointmentService getAppointmentService() {
		return appointmentService;
	}

	public void setAppointmentService(AppointmentService appointmentService) {
		this.appointmentService = appointmentService;
	}

	public List<AppointmentDto> getLazyAppointmentList() {
		return lazyAppointmentList;
	}

	public void setLazyAppointmentList(List<AppointmentDto> lazyAppointmentList) {
		this.lazyAppointmentList = lazyAppointmentList;
	}

	public Long getAppointmentsCount() {
		return appointmentsCount;
	}

	public void setAppointmentsCount(Long appointmentsCount) {
		this.appointmentsCount = appointmentsCount;
	}

	public Map<String, Object> getFilterFields() {
		return filterFields;
	}

	public void setFilterFields(Map<String, Object> filterFields) {
		this.filterFields = filterFields;
	}

	public Map<String, String> getSortFields() {
		return sortFields;
	}

	public void setSortFields(Map<String, String> sortFields) {
		this.sortFields = sortFields;
	}

	public AppointmentTransformer getAppointmentTransformer() {
		return appointmentTransformer;
	}

	public void setAppointmentTransformer(AppointmentTransformer appointmentTransformer) {
		this.appointmentTransformer = appointmentTransformer;
	}

	public PatientTransformer getPatientTransformer() {
		return patientTransformer;
	}

	public void setPatientTransformer(PatientTransformer patientTransformer) {
		this.patientTransformer = patientTransformer;
	}

	public DoctorTransformer getDoctorTransformer() {
		return doctorTransformer;
	}

	public void setDoctorTransformer(DoctorTransformer doctorTransformer) {
		this.doctorTransformer = doctorTransformer;
	}

}