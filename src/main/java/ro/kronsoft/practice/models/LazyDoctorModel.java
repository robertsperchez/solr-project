package ro.kronsoft.practice.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.PrimeFaces;
import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import ro.kronsoft.practice.dto.DoctorDto;
import ro.kronsoft.practice.entities.Doctor;
import ro.kronsoft.practice.services.DoctorService;
import ro.kronsoft.practice.transformers.DoctorTransformer;

@ManagedBean(name = "lazyDoctorModel")
@ViewScoped
public class LazyDoctorModel extends LazyDataModel<DoctorDto> {

	private static final long serialVersionUID = -4872536753164157776L;

	static final String constFirstName = "firstName";
	static final String constLastName = "lastName";
	static final String constEmail = "email";
	static final String constPhoneNumber = "phoneNumber";
	static final String constSpecialtyType = "specialtyType";

	@ManagedProperty(value = "#{doctorService}")
	private transient DoctorService doctorService;

	@ManagedProperty(value = "#{doctorTransformer}")
	private DoctorTransformer doctorTransformer;

	private Map<String, Object> filterFields;

	private Map<String, String> sortFields;

	private Long doctorsCount;

	public LazyDoctorModel() {
		filterFields = new HashMap<>();
		sortFields = new HashMap<>();
	}

	@Override
	public List<DoctorDto> load(int first, int pageSize, Map<String, SortMeta> sortMeta,
			Map<String, FilterMeta> filterMeta) {

		List<Doctor> data = new ArrayList<>();
		String sortKey = new String();
		String sortOrder = new String();

		for (String k : sortMeta.keySet()) {
			sortKey = k;
		}
		for (SortMeta s : sortMeta.values()) {
			sortOrder = s.getSortOrder().toString();
		}

		for (FilterMeta meta : filterMeta.values()) {
			String filterField = meta.getFilterField();
			Object filterValue = meta.getFilterValue();
			if (filterValue != null && filterValue.toString().length() > 1000) {
				filterValue = null;
				PrimeFaces current = PrimeFaces.current();
				current.executeScript("PF('filterErrorDialog').show();");
			}
			filterDataTable(filterField, filterValue);
		}
		for (SortMeta meta : sortMeta.values()) {
			String sortField = meta.getSortField();
			String sortOrderString = meta.getSortOrder().toString();
			sortDataTable(sortField, sortOrderString);
		}

		if (!filterFields.values().isEmpty()) {
			data = doctorService.filterDoctors(filterFields, sortFields, PageRequest.of(first / pageSize, pageSize));
			this.setRowCount(data.size());
		} else if (sortKey.isEmpty() && filterFields.isEmpty()) {
			data = doctorService.findAllLazyLoading(first / pageSize, pageSize);
			this.setRowCount(Math.toIntExact(this.getDoctorsCount()));
		} else if (sortOrder.equals("ASCENDING")) {
			data = doctorService.sortAllLazyLoading(first / pageSize, pageSize, Sort.by(sortKey).ascending());
			this.setRowCount(Math.toIntExact(this.getDoctorsCount()));
		} else if (sortOrder.equals("DESCENDING")) {
			data = doctorService.sortAllLazyLoading(first / pageSize, pageSize, Sort.by(sortKey).descending());
			this.setRowCount(Math.toIntExact(this.getDoctorsCount()));
		}
		this.doctorsCount = (long) data.size();
		return doctorTransformer.toDtoList(data);
	}

	/**
	 * Fills a map object with the selected sort fields and order.
	 * 
	 * @param sortKey   The sort field.
	 * @param sortOrder
	 */
	public void sortDataTable(final String sortKey, final String sortOrder) {
		switch (sortKey) {
		case constFirstName:
			if (sortOrder != null) {
				sortFields.put(constFirstName, sortOrder);
			} else {
				sortFields.remove(constFirstName);
			}
			break;
		case constLastName:
			if (sortOrder != null) {
				sortFields.put(constLastName, sortOrder);
			} else {
				sortFields.remove(constLastName);
			}
			break;
		case constEmail:
			if (sortOrder != null) {
				sortFields.put(constEmail, sortOrder);
			} else {
				sortFields.remove(constEmail);
			}
			break;
		case constPhoneNumber:
			if (sortOrder != null) {
				sortFields.put(constPhoneNumber, sortOrder);
			} else {
				sortFields.remove(constPhoneNumber);
			}
			break;
		case constSpecialtyType:
			if (sortOrder != null) {
				sortFields.put(constSpecialtyType, sortOrder);
			} else {
				sortFields.remove(constSpecialtyType);
			}
			break;
		default:
			break;
		}
	}

	/**
	 * Puts the selected filter fields and values into a map object.
	 * 
	 * @param filterField
	 * @param filterValue
	 */
	public void filterDataTable(final String filterField, final Object filterValue) {
		switch (filterField) {
		case constFirstName:
			if (filterValue != null) {
				filterFields.put(constFirstName, filterValue.toString());
			} else {
				filterFields.remove(constFirstName);
			}
			break;
		case constLastName:
			if (filterValue != null) {
				filterFields.put(constLastName, filterValue.toString());
			} else {
				filterFields.remove(constLastName);
			}
			break;
		case constEmail:
			if (filterValue != null) {
				filterFields.put(constEmail, filterValue.toString());
			} else {
				filterFields.remove(constEmail);
			}
			break;
		case constPhoneNumber:
			if (filterValue != null) {
				filterFields.put(constPhoneNumber, filterValue.toString());
			} else {
				filterFields.remove(constPhoneNumber);
			}
			break;
		case constSpecialtyType:
			if (filterValue != null) {
				filterFields.put(constSpecialtyType, filterValue.toString());
			} else {
				filterFields.remove(constSpecialtyType);
			}
			break;
		default:
			break;
		}
	}

	public DoctorService getDoctorService() {
		return doctorService;
	}

	public void setDoctorService(DoctorService doctorService) {
		this.doctorService = doctorService;
	}

	public Long getDoctorsCount() {
		return doctorsCount;
	}

	public void setDoctorsCount(Long doctorsCount) {
		this.doctorsCount = doctorsCount;
	}

	public Map<String, Object> getFilterFields() {
		return filterFields;
	}

	public void setFilterFields(Map<String, Object> filterFields) {
		this.filterFields = filterFields;
	}

	public Map<String, String> getSortFields() {
		return sortFields;
	}

	public void setSortFields(Map<String, String> sortFields) {
		this.sortFields = sortFields;
	}

	public DoctorTransformer getDoctorTransformer() {
		return doctorTransformer;
	}

	public void setDoctorTransformer(DoctorTransformer doctorTransformer) {
		this.doctorTransformer = doctorTransformer;
	}
}
