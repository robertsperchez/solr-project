package ro.kronsoft.practice.models;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import ro.kronsoft.practice.dto.DoctorDto;
import ro.kronsoft.practice.entities.enums.SpecialtyType;

@ManagedBean(name = "doctorModel")
@ViewScoped
public class DoctorModel implements Serializable {

	private static final long serialVersionUID = 4458734991373031500L;

	private List<SpecialtyType> specialtyTypeList;
	
	private DoctorDto doctor;

	public DoctorModel() {
		super();
	}
	
	@PostConstruct
	public void init() {
		specialtyTypeList = Arrays.asList(SpecialtyType.values());
		doctor = new DoctorDto();
	}

	public List<SpecialtyType> getSpecialtyTypeList() {
		return specialtyTypeList;
	}

	public void setSpecialtyTypeList(List<SpecialtyType> specialtyTypeList) {
		this.specialtyTypeList = specialtyTypeList;
	}

	public DoctorDto getDoctor() {
		return doctor;
	}

	public void setDoctor(DoctorDto doctor) {
		this.doctor = doctor;
	}
}
