package ro.kronsoft.practice.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.PrimeFaces;
import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import ro.kronsoft.practice.dto.PatientDto;
import ro.kronsoft.practice.entities.Patient;
import ro.kronsoft.practice.services.PatientService;
import ro.kronsoft.practice.transformers.PatientTransformer;

@ManagedBean(name = "lazyPatientModel")
@ViewScoped
public class LazyPatientModel extends LazyDataModel<PatientDto> {

	private static final long serialVersionUID = 8372046764178150049L;

	static final String constFirstName = "firstName";
	static final String constLastName = "lastName";
	static final String constCnp = "cnp";
	static final String constSexType = "sexType";
	static final String constBirthDate = "birthDate";
	static final String constPhoneNumber = "phoneNumber";
	static final String constCountry = "country";
	static final String constCity = "city";

	@ManagedProperty(value = "#{patientService}")
	private transient PatientService patientService;

	@ManagedProperty(value = "#{patientTransformer}")
	private PatientTransformer patientTransformer;

	private Map<String, Object> filterFields;

	private Map<String, String> sortFields;

	private Long patientsCount;

	public LazyPatientModel() {
		filterFields = new HashMap<>();
		sortFields = new HashMap<>();
	}

	@Override
	public List<PatientDto> load(int first, int pageSize, Map<String, SortMeta> sortMeta,
			Map<String, FilterMeta> filterMeta) {

		List<Patient> data = new ArrayList<>();
		String sortKey = new String();
		String sortOrder = new String();

		for (String k : sortMeta.keySet()) {
			sortKey = k;
		}
		for (SortMeta s : sortMeta.values()) {
			sortOrder = s.getSortOrder().toString();
		}

		for (FilterMeta meta : filterMeta.values()) {
			String filterField = meta.getFilterField();
			Object filterValue = meta.getFilterValue();
			if (filterValue != null && filterValue.toString().length() > 1000) {
				filterValue = null;
				PrimeFaces current = PrimeFaces.current();
				current.executeScript("PF('filterErrorDialog').show();");
			}
			filterDataTable(filterField, filterValue);
		}
		for (SortMeta meta : sortMeta.values()) {
			String sortField = meta.getSortField();
			String sortOrderString = meta.getSortOrder().toString();
			sortDataTable(sortField, sortOrderString);
		}

		if (!filterFields.values().isEmpty()) {
			data = patientService.filterPatients(filterFields, sortFields, PageRequest.of(first / pageSize, pageSize));
			this.setRowCount(data.size());
		} else if (sortKey.isEmpty() && filterFields.isEmpty()) {
			data = patientService.findAllLazyLoading(first / pageSize, pageSize);
			this.setRowCount(Math.toIntExact(this.getPatientsCount()));
		} else if (sortOrder.equals("ASCENDING")) {
			data = patientService.sortAllLazyLoading(first / pageSize, pageSize, Sort.by(sortKey).ascending());
			this.setRowCount(Math.toIntExact(this.getPatientsCount()));
		} else if (sortOrder.equals("DESCENDING")) {
			data = patientService.sortAllLazyLoading(first / pageSize, pageSize, Sort.by(sortKey).descending());
			this.setRowCount(Math.toIntExact(this.getPatientsCount()));
		}
		this.patientsCount = (long) data.size();
		return patientTransformer.toDtoList(data);
	}

	/**
	 * Fills a map object with the selected sort fields and order.
	 * 
	 * @param sortKey   The sort field.
	 * @param sortOrder
	 */
	public void sortDataTable(final String sortKey, final String sortOrder) {
		switch (sortKey) {
		case constFirstName:
			if (sortOrder != null) {
				sortFields.put(constFirstName, sortOrder);
			} else {
				sortFields.remove(constFirstName);
			}
			break;
		case constLastName:
			if (sortOrder != null) {
				sortFields.put(constLastName, sortOrder);
			} else {
				sortFields.remove(constLastName);
			}
			break;
		case constCnp:
			if (sortOrder != null) {
				sortFields.put(constCnp, sortOrder);
			} else {
				sortFields.remove(constCnp);
			}
			break;
		case constSexType:
			if (sortOrder != null) {
				sortFields.put(constSexType, sortOrder);
			} else {
				sortFields.remove(constSexType);
			}
			break;
		case constBirthDate:
			if (sortOrder != null) {
				sortFields.put(constBirthDate, sortOrder);
			} else {
				sortFields.remove(constBirthDate);
			}
			break;
		case constPhoneNumber:
			if (sortOrder != null) {
				sortFields.put(constPhoneNumber, sortOrder);
			} else {
				sortFields.remove(constPhoneNumber);
			}
			break;
		case constCountry:
			if (sortOrder != null) {
				sortFields.put(constCountry, sortOrder);
			} else {
				sortFields.remove(constCountry);
			}
			break;
		case constCity:
			if (sortOrder != null) {
				sortFields.put(constCity, sortOrder);
			} else {
				sortFields.remove(constCity);
			}
			break;
		default:
			break;
		}
	}

	/**
	 * Puts the selected filter fields and values into a map object.
	 * 
	 * @param filterField
	 * @param filterValue
	 */
	public void filterDataTable(final String filterField, final Object filterValue) {
		switch (filterField) {
		case constFirstName:
			if (filterValue != null) {
				filterFields.put(constFirstName, filterValue.toString());
			} else {
				filterFields.remove(constFirstName);
			}
			break;
		case constLastName:
			if (filterValue != null) {
				filterFields.put(constLastName, filterValue.toString());
			} else {
				filterFields.remove(constLastName);
			}
			break;
		case constCnp:
			if (filterValue != null) {
				filterFields.put(constCnp, filterValue.toString());
			} else {
				filterFields.remove(constCnp);
			}
			break;
		case constSexType:
			if (filterValue != null) {
				filterFields.put(constSexType, filterValue.toString());
			} else {
				filterFields.remove(constSexType);
			}
			break;
		case constBirthDate:
			if (filterValue != null) {
				filterFields.put(constBirthDate, filterValue);
			} else {
				filterFields.remove(constBirthDate);
			}
			break;
		case constPhoneNumber:
			if (filterValue != null) {
				filterFields.put(constPhoneNumber, filterValue.toString());
			} else {
				filterFields.remove(constPhoneNumber);
			}
			break;
		case constCountry:
			if (filterValue != null) {
				filterFields.put(constCountry, filterValue.toString());
			} else {
				filterFields.remove(constCountry);
			}
			break;
		case constCity:
			if (filterValue != null) {
				filterFields.put(constCity, filterValue.toString());
			} else {
				filterFields.remove(constCity);
			}
			break;
		default:
			break;
		}
	}

	public Long getPatientsCount() {
		return patientsCount;
	}

	public void setPatientsCount(Long patientsCount) {
		this.patientsCount = patientsCount;
	}

	public PatientService getPatientService() {
		return patientService;
	}

	public void setPatientService(PatientService patientService) {
		this.patientService = patientService;
	}

	public Map<String, Object> getFilterFields() {
		return filterFields;
	}

	public void setFilterFields(Map<String, Object> filterFields) {
		this.filterFields = filterFields;
	}

	public Map<String, String> getSortFields() {
		return sortFields;
	}

	public void setSortFields(Map<String, String> sortFields) {
		this.sortFields = sortFields;
	}

	public PatientTransformer getPatientTransformer() {
		return patientTransformer;
	}

	public void setPatientTransformer(PatientTransformer patientTransformer) {
		this.patientTransformer = patientTransformer;
	}
}
