package ro.kronsoft.practice.models;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import ro.kronsoft.practice.dto.AppointmentDto;
import ro.kronsoft.practice.entities.enums.AppointmentStatus;
import ro.kronsoft.practice.entities.enums.AppointmentType;
import ro.kronsoft.practice.entities.enums.SpecialtyType;

@ManagedBean(name = "appointmentModel")
@ViewScoped
public class AppointmentModel implements Serializable {

	private static final long serialVersionUID = 5575871881710639592L;

	private List<AppointmentStatus> statusList;

	private List<AppointmentType> typeList;

	private List<SpecialtyType> specialtyList;

	private AppointmentDto appointment;

	public AppointmentModel() {
		super();
	}

	@PostConstruct
	public void init() {
		statusList = Arrays.asList(AppointmentStatus.values());
		typeList = Arrays.asList(AppointmentType.values());
		specialtyList = Arrays.asList(SpecialtyType.values());
		appointment = new AppointmentDto();
	}

	public AppointmentDto getAppointment() {
		return appointment;
	}

	public void setAppointment(AppointmentDto appointment) {
		this.appointment = appointment;
	}

	public List<AppointmentStatus> getStatusList() {
		return statusList;
	}

	public void setStatusList(List<AppointmentStatus> statusList) {
		this.statusList = statusList;
	}

	public List<AppointmentType> getTypeList() {
		return typeList;
	}

	public void setTypeList(List<AppointmentType> typeList) {
		this.typeList = typeList;
	}

	public List<SpecialtyType> getSpecialtyList() {
		return specialtyList;
	}

	public void setSpecialtyList(List<SpecialtyType> specialtyList) {
		this.specialtyList = specialtyList;
	}
}
