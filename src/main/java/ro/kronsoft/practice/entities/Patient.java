package ro.kronsoft.practice.entities;

import java.time.LocalDate;

import org.springframework.data.solr.core.mapping.Indexed;
import org.springframework.data.solr.core.mapping.SolrDocument;

import ro.kronsoft.practice.entities.enums.SexTypeEnum;

@SolrDocument(collection = "patients")
public class Patient extends BaseEntity{

	private static final long serialVersionUID = -817128320160521461L;

	@Indexed(name = "firstName_s", type = "string")
	private String firstName;

	@Indexed(name = "lastName_s", type = "string")
	private String lastName;
	
	@Indexed(name = "fullName_s", type = "string")
	private String fullName;

	@Indexed(name = "cnp_s", type = "string")
	private String cnp;

	@Indexed(name = "sexType_s", type = "string")
	private SexTypeEnum sexType;

	@Indexed(name = "birthDate_dt", type = "pdate")
	private LocalDate birthDate;

	@Indexed(name = "phoneNumber_s", type = "string")
	private String phoneNumber;

	@Indexed(name = "country_s", type = "string")
	private String country;

	@Indexed(name = "city_s", type = "string")
	private String city;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCnp() {
		return cnp;
	}

	public void setCnp(String cnp) {
		this.cnp = cnp;
	}

	public SexTypeEnum getSexType() {
		return sexType;
	}

	public void setSexType(SexTypeEnum sexType) {
		this.sexType = sexType;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Patient(String id, String firstName, String lastName, String cnp, SexTypeEnum sexType,
			LocalDate birthDate, String phoneNumber, String country, String city) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.cnp = cnp;
		this.sexType = sexType;
		this.birthDate = birthDate;
		this.phoneNumber = phoneNumber;
		this.country = country;
		this.city = city;
	}

	public Patient() {
		super();
	}

	@Override
	public String toString() {
		return "Patient [firstName=" + firstName + ", lastName=" + lastName + ", fullName=" + fullName + ", cnp=" + cnp
				+ ", sexType=" + sexType + ", birthDate=" + birthDate + ", phoneNumber=" + phoneNumber + ", country="
				+ country + ", city=" + city + "]";
	}

}
