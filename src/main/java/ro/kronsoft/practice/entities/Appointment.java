package ro.kronsoft.practice.entities;

import java.time.LocalDate;

import org.springframework.data.solr.core.mapping.Indexed;
import org.springframework.data.solr.core.mapping.SolrDocument;

import ro.kronsoft.practice.entities.enums.AppointmentStatus;
import ro.kronsoft.practice.entities.enums.AppointmentType;
import ro.kronsoft.practice.entities.enums.SpecialtyType;

@SolrDocument(collection = "appointments")
public class Appointment extends BaseEntity {

	private static final long serialVersionUID = -8555382289535846957L;

	@Indexed(name = "appointmentType_s", type = "string")
	private AppointmentType appointmentType;

	@Indexed(name = "appointmentStatus_s", type = "string")
	private AppointmentStatus appointmentStatus;

	@Indexed(name = "specialtyType_s", type = "string")
	private SpecialtyType specialtyType;

	@Indexed(name = "date_dt", type = "pdate")
	private LocalDate date;

	@Indexed(name = "startTime_s", type = "string")
	private String startTime;

	@Indexed(name = "endTime_s", type = "string")
	private String endTime;

	@Indexed(name = "description_s", type = "string")
	private String description;

	@Indexed(name = "patientId_s", type = "string")
	private String patientId;

	@Indexed(name = "doctorId_s", type = "string")
	private String doctorId;

	public AppointmentType getAppointmentType() {
		return appointmentType;
	}

	public void setAppointmentType(AppointmentType appointmentType) {
		this.appointmentType = appointmentType;
	}

	public AppointmentStatus getAppointmentStatus() {
		return appointmentStatus;
	}

	public void setAppointmentStatus(AppointmentStatus appointmentStatus) {
		this.appointmentStatus = appointmentStatus;
	}

	public SpecialtyType getSpecialtyType() {
		return specialtyType;
	}

	public void setSpecialtyType(SpecialtyType specialtyType) {
		this.specialtyType = specialtyType;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public String getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(String doctorId) {
		this.doctorId = doctorId;
	}

	@Override
	public String toString() {
		return "Appointment [appointmentType=" + appointmentType + ", appointmentStatus=" + appointmentStatus
				+ ", specialtyType=" + specialtyType + ", date=" + date + ", startTime=" + startTime + ", endTime="
				+ endTime + ", description=" + description + ", patientId=" + patientId + ", doctorId=" + doctorId
				+ "]";
	}

	public Appointment(AppointmentType appointmentType, AppointmentStatus appointmentStatus,
			SpecialtyType specialtyType, LocalDate date, String startTime, String endTime, String description,
			String patientId, String doctorId) {
		super();
		this.appointmentType = appointmentType;
		this.appointmentStatus = appointmentStatus;
		this.specialtyType = specialtyType;
		this.date = date;
		this.startTime = startTime;
		this.endTime = endTime;
		this.description = description;
		this.patientId = patientId;
		this.doctorId = doctorId;
	}

	public Appointment() {
		super();
	}
}
