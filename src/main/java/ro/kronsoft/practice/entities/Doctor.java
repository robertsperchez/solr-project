package ro.kronsoft.practice.entities;

import org.springframework.data.solr.core.mapping.Indexed;
import org.springframework.data.solr.core.mapping.SolrDocument;

import ro.kronsoft.practice.entities.enums.SpecialtyType;

@SolrDocument(collection = "doctors")
public class Doctor extends BaseEntity {

	private static final long serialVersionUID = 7173930441296051001L;

	@Indexed(name = "firstName_s", type = "string")
	private String firstName;

	@Indexed(name = "lastName_s", type = "string")
	private String lastName;
	
	@Indexed(name = "fullName_s", type = "string")
	private String fullName;
	
	@Indexed(name = "specialtyType_s", type = "string")
	private SpecialtyType specialtyType;
	
	@Indexed(name = "phoneNumber_s", type = "string")
	private String phoneNumber;
	
	@Indexed(name = "email_s", type = "string")
	private String email;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public SpecialtyType getSpecialtyType() {
		return specialtyType;
	}

	public void setSpecialtyType(SpecialtyType specialtyType) {
		this.specialtyType = specialtyType;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Doctor(String firstName, String lastName, String fullName, SpecialtyType specialtyType, String phoneNumber,
			String email) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.specialtyType = specialtyType;
		this.phoneNumber = phoneNumber;
		this.email = email;
	}
	
	public Doctor() {
		super();
	}

	@Override
	public String toString() {
		return "Doctor [firstName=" + firstName + ", lastName=" + lastName + ", fullName=" + fullName
				+ ", specialtyType=" + specialtyType + ", phoneNumber=" + phoneNumber + ", email=" + email + "]";
	}
	
}
