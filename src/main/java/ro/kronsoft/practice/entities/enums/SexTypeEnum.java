package ro.kronsoft.practice.entities.enums;

public enum SexTypeEnum {
	MALE, 
	FEMALE, 
	UNDEFINED;
}