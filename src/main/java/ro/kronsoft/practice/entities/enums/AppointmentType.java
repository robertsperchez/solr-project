package ro.kronsoft.practice.entities.enums;

public enum AppointmentType {
	REGULAR, HOLIDAY, VACATION, GROUP;
}
