package ro.kronsoft.practice.entities.enums;

public enum AppointmentStatus {
	CREATED,
	PLANNED,
	CONFIRMED,
	CLOSED,
	CANCELLED;
}

