package ro.kronsoft.practice.entities;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.Indexed;

public class BaseEntity implements Serializable {

	private static final long serialVersionUID = 2527855291625675859L;

	@Id
	@Indexed(name = "id", type = "string")
	protected String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
