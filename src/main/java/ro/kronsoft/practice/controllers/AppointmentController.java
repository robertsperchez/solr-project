package ro.kronsoft.practice.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.PrimeFaces;
import org.springframework.context.annotation.ComponentScan;

import ro.kronsoft.practice.dto.AppointmentDto;
import ro.kronsoft.practice.dto.DoctorDto;
import ro.kronsoft.practice.dto.PatientDto;
import ro.kronsoft.practice.entities.enums.SpecialtyType;
import ro.kronsoft.practice.models.AppointmentModel;
import ro.kronsoft.practice.models.LazyAppointmentModel;
import ro.kronsoft.practice.services.AppointmentService;
import ro.kronsoft.practice.services.DoctorService;
import ro.kronsoft.practice.services.PatientService;
import ro.kronsoft.practice.transformers.AppointmentTransformer;
import ro.kronsoft.practice.transformers.DoctorTransformer;
import ro.kronsoft.practice.transformers.PatientTransformer;

@ManagedBean(name = "appointmentController")
@ViewScoped
@ComponentScan("ro.kronsoft.practice")
public class AppointmentController {

	@ManagedProperty(value = "#{appointmentService}")
	private AppointmentService appointmentService;

	@ManagedProperty(value = "#{lazyAppointmentModel}")
	private LazyAppointmentModel lazyAppointmentModel;

	@ManagedProperty(value = "#{appointmentModel}")
	private AppointmentModel appointmentModel;

	@ManagedProperty(value = "#{patientService}")
	private PatientService patientService;

	@ManagedProperty(value = "#{doctorService}")
	private DoctorService doctorService;

	@ManagedProperty(value = "#{appointmentTransformer}")
	private AppointmentTransformer appointmentTransformer;

	@ManagedProperty(value = "#{doctorTransformer}")
	private DoctorTransformer doctorTransformer;

	@ManagedProperty(value = "#{patientTransformer}")
	private PatientTransformer patientTransformer;

	private PatientDto patient;

	private DoctorDto doctor;

	private SpecialtyType specialtyTypeAutocomplete;

	private List<PatientDto> patientsQueryList;

	private List<DoctorDto> doctorsQueryList;

	public AppointmentController() {
		// Empty constructor
	}

	@PostConstruct
	public void init() {
		lazyAppointmentModel.setAppointmentsCount(appointmentService.countAllAppointments());
		patientsQueryList = new ArrayList<>();
		doctorsQueryList = new ArrayList<>();
	}

	/**
	 * Method used to autocomplete patients for the p:autocomplete input.
	 * 
	 * @param query String that is used to search for patients.
	 * @return Returns a list of filtered patients based on the query.
	 */
	public List<PatientDto> completePatient(final String query) {
		String queryLowerCase = query.toLowerCase();
		patientsQueryList = patientTransformer.toDtoList(patientService.findByFullName(queryLowerCase));
		return patientsQueryList.stream().filter(p -> p.getFullName().toLowerCase().startsWith(queryLowerCase))
				.collect(Collectors.toList());
	}

	/**
	 * Sets the specialty type that is then used for the autocomplete.
	 */
	public void setSpecialtyForAutocomplete() {
		if (appointmentModel.getAppointment().getSpecialtyType() != null)
			specialtyTypeAutocomplete = appointmentModel.getAppointment().getSpecialtyType();
	}

	/**
	 * Method used to autocomplete doctors for the p:autocomplete input. It uses the
	 * specialty type to only autocomplete doctors of that specialty.
	 * 
	 * @param query String that is used to search for doctors.
	 * @return Returns a list of filtered doctors based on the query.
	 */
	public List<DoctorDto> completeDoctor(final String query) {
		String queryLowerCase = query.toLowerCase();
		if (specialtyTypeAutocomplete != null) {
			doctorsQueryList = doctorTransformer
					.toDtoList(doctorService.findByFullNameAndSpecialty(queryLowerCase, specialtyTypeAutocomplete));
		} else {
			doctorsQueryList = doctorTransformer.toDtoList(doctorService.findByFullNameStartingWith(queryLowerCase));
		}
		return doctorsQueryList.stream().filter(p -> p.getFullName().toLowerCase().startsWith(queryLowerCase))
				.collect(Collectors.toList());
	}

	/**
	 * Method used to autocomplete doctors for the p:autocomplete input.
	 * 
	 * @param query String that is used to search for doctors.
	 * @return Returns a list of filtered doctors based on the query.
	 */
	public List<DoctorDto> completeDoctorFilter(String query) {
		String queryLowerCase = query.toLowerCase();
		doctorsQueryList = doctorTransformer.toDtoList(doctorService.findByFullNameStartingWith(queryLowerCase));
		return doctorsQueryList.stream().filter(p -> p.getFullName().toLowerCase().startsWith(queryLowerCase))
				.collect(Collectors.toList());
	}

	/**
	 * Function called when the add new appointment button is clicked.
	 */
	public void addClicked() {
		doctor = null;
		patient = null;
		appointmentModel.setAppointment(new AppointmentDto());
		specialtyTypeAutocomplete = null;
		PrimeFaces current = PrimeFaces.current();
		current.executeScript("PF('appointmentFormDialog').show();");
	}

	/**
	 * Function called when the edit appointment button is clicked.
	 * 
	 * @param appointment The appointment that is chosen to be edited.
	 */
	public void editClicked(final AppointmentDto appointment) {
		appointmentModel.setAppointment(appointment);
		patient = patientTransformer.toDto(patientService.getPatientById(appointment.getPatientId()));
		doctor = doctorTransformer.toDto(doctorService.findDoctorById(appointment.getDoctorId()));
		specialtyTypeAutocomplete = null;
		PrimeFaces current = PrimeFaces.current();
		current.executeScript("PF('appointmentFormDialog').show();");
	}

	/**
	 * Function called when the view appointment button is clicked.
	 * 
	 * @param appointment The appointment chosen to be viewed.
	 */
	public void viewClicked(final AppointmentDto appointment) {
		appointmentModel.setAppointment(appointment);
		PrimeFaces current = PrimeFaces.current();
		current.executeScript("PF('appointmentProfileDialog').show();");
	}

	/**
	 * Function called when the save appointment button is clicked.
	 */
	public void createAppointment() {
		if (appointmentModel.getAppointment().getId().isEmpty()) {
			appointmentModel.getAppointment().setId(null);
		}
		appointmentModel.getAppointment().setPatientId(patient.getId());
		appointmentModel.getAppointment().setDoctorId(doctor.getId());
		appointmentService.saveAppointment(appointmentTransformer.toEntity(appointmentModel.getAppointment()));
		lazyAppointmentModel.setAppointmentsCount(appointmentService.countAllAppointments());
		specialtyTypeAutocomplete = null;
		PrimeFaces current = PrimeFaces.current();
		current.executeScript("PF('appointmentFormDialog').hide();");
	}

	/**
	 * Function called when the delete appointment button is clicked.
	 * 
	 * @param id The id of the appointment that will be deleted.
	 */
	public void deleteAppointment(final String id) {
		appointmentService.deleteAppointment(id);
		lazyAppointmentModel.setAppointmentsCount(appointmentService.countAllAppointments());
	}

	public DoctorService getDoctorService() {
		return doctorService;
	}

	public List<DoctorDto> getDoctorsQueryList() {
		return doctorsQueryList;
	}

	public void setDoctorsQueryList(List<DoctorDto> doctorsQueryList) {
		this.doctorsQueryList = doctorsQueryList;
	}

	public void setDoctorService(DoctorService doctorService) {
		this.doctorService = doctorService;
	}

	public DoctorDto getDoctor() {
		return doctor;
	}

	public void setDoctor(DoctorDto doctor) {
		this.doctor = doctor;
	}

	public List<PatientDto> getPatientsQueryList() {
		return patientsQueryList;
	}

	public void setPatientsQueryList(List<PatientDto> patientsQueryList) {
		this.patientsQueryList = patientsQueryList;
	}

	public PatientService getPatientService() {
		return patientService;
	}

	public void setPatientService(PatientService patientService) {
		this.patientService = patientService;
	}

	public LazyAppointmentModel getLazyAppointmentModel() {
		return lazyAppointmentModel;
	}

	public void setLazyAppointmentModel(LazyAppointmentModel lazyAppointmentModel) {
		this.lazyAppointmentModel = lazyAppointmentModel;
	}

	public AppointmentService getAppointmentService() {
		return appointmentService;
	}

	public void setAppointmentService(AppointmentService appointmentService) {
		this.appointmentService = appointmentService;
	}

	public AppointmentModel getAppointmentModel() {
		return appointmentModel;
	}

	public void setAppointmentModel(AppointmentModel patientModel) {
		this.appointmentModel = patientModel;
	}

	public PatientDto getPatient() {
		return patient;
	}

	public void setPatient(PatientDto patient) {
		this.patient = patient;
	}

	public SpecialtyType getSpecialtyTypeAutocomplete() {
		return specialtyTypeAutocomplete;
	}

	public void setSpecialtyTypeAutocomplete(SpecialtyType specialtyTypeAutocomplete) {
		this.specialtyTypeAutocomplete = specialtyTypeAutocomplete;
	}

	public AppointmentTransformer getAppointmentTransformer() {
		return appointmentTransformer;
	}

	public void setAppointmentTransformer(AppointmentTransformer appointmentTransformer) {
		this.appointmentTransformer = appointmentTransformer;
	}

	public DoctorTransformer getDoctorTransformer() {
		return doctorTransformer;
	}

	public void setDoctorTransformer(DoctorTransformer doctorTransformer) {
		this.doctorTransformer = doctorTransformer;
	}

	public PatientTransformer getPatientTransformer() {
		return patientTransformer;
	}

	public void setPatientTransformer(PatientTransformer patientTransformer) {
		this.patientTransformer = patientTransformer;
	}

}
