package ro.kronsoft.practice.controllers;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.view.ViewScoped;

import org.primefaces.PrimeFaces;
import org.springframework.context.annotation.ComponentScan;

import ro.kronsoft.practice.dto.DoctorDto;
import ro.kronsoft.practice.models.DoctorModel;
import ro.kronsoft.practice.models.LazyDoctorAppointmentModel;
import ro.kronsoft.practice.models.LazyDoctorModel;
import ro.kronsoft.practice.services.AppointmentService;
import ro.kronsoft.practice.services.DoctorService;
import ro.kronsoft.practice.transformers.DoctorTransformer;

@ManagedBean(name = "doctorController")
@ViewScoped
@ComponentScan("ro.kronsoft.practice")
public class DoctorController {

	@ManagedProperty(value = "#{doctorService}")
	private DoctorService doctorService;

	@ManagedProperty(value = "#{doctorTransformer}")
	private DoctorTransformer doctorTransformer;

	@ManagedProperty(value = "#{doctorModel}")
	private DoctorModel doctorModel;

	@ManagedProperty(value = "#{lazyDoctorModel}")
	private LazyDoctorModel lazyDoctorModel;

	@ManagedProperty(value = "#{lazyDoctorAppointmentModel}")
	private LazyDoctorAppointmentModel lazyDoctorAppointmentModel;

	@ManagedProperty(value = "#{appointmentService}")
	private AppointmentService appointmentService;

	public DoctorController() {
		// Empty constructor
	}

	@PostConstruct
	public void init() {
		lazyDoctorModel.setDoctorsCount(doctorService.countAllDoctors());
	}

	/**
	 * Function called when the add new doctor button is clicked.
	 */
	public void addClicked() {
		doctorModel.setDoctor(new DoctorDto());
		PrimeFaces current = PrimeFaces.current();
		current.executeScript("PF('doctorFormDialog').show();");
	}

	/**
	 * Function called when the edit doctor button is clicked.
	 * 
	 * @param doctor The doctor that is chosen to be edited.
	 */
	public void editClicked(final DoctorDto doctor) {
		doctorModel.setDoctor(doctor);
		PrimeFaces current = PrimeFaces.current();
		current.executeScript("PF('doctorFormDialog').show();");
	}

	/**
	 * Function called when the view doctor button is clicked.
	 * 
	 * @param doctor The doctor that is chosen to be viewed.
	 */
	public void viewClicked(final DoctorDto doctor) {
		doctorModel.setDoctor(doctor);
		lazyDoctorAppointmentModel.setDoctorId(doctor.getId());
		PrimeFaces current = PrimeFaces.current();
		current.executeScript("PF('doctorsProfileDialog').show();");
	}

	/**
	 * Function called when the save appointment button is clicked.
	 */
	public void createDoctor() {
		if (doctorModel.getDoctor().getId().isEmpty()) {
			doctorModel.getDoctor().setId(null);
		}
		doctorService.saveDoctor(doctorTransformer.toEntity(doctorModel.getDoctor()));
		lazyDoctorModel.setDoctorsCount(doctorService.countAllDoctors());
		PrimeFaces current = PrimeFaces.current();
		current.executeScript("PF('doctorFormDialog').hide();");
	}

	/**
	 * Function called when the delete doctor button is clicked.
	 * 
	 * @param id The id of the doctor that will be deleted.
	 */
	public void deleteDoctor(String id) {
		doctorService.deleteDoctor(id);
		appointmentService.deleteAllByDoctorId(id);
		lazyDoctorModel.setDoctorsCount(doctorService.countAllDoctors());
	}

	public DoctorService getDoctorService() {
		return doctorService;
	}

	public DoctorModel getDoctorModel() {
		return doctorModel;
	}

	public void setDoctorModel(DoctorModel doctorModel) {
		this.doctorModel = doctorModel;
	}

	public void setDoctorService(DoctorService doctorService) {
		this.doctorService = doctorService;
	}

	public LazyDoctorModel getLazyDoctorModel() {
		return lazyDoctorModel;
	}

	public void setLazyDoctorModel(LazyDoctorModel lazyDoctorModel) {
		this.lazyDoctorModel = lazyDoctorModel;
	}

	public LazyDoctorAppointmentModel getLazyDoctorAppointmentModel() {
		return lazyDoctorAppointmentModel;
	}

	public void setLazyDoctorAppointmentModel(LazyDoctorAppointmentModel lazyDoctorAppointmentModel) {
		this.lazyDoctorAppointmentModel = lazyDoctorAppointmentModel;
	}

	public DoctorTransformer getDoctorTransformer() {
		return doctorTransformer;
	}

	public void setDoctorTransformer(DoctorTransformer doctorTransformer) {
		this.doctorTransformer = doctorTransformer;
	}

	public AppointmentService getAppointmentService() {
		return appointmentService;
	}

	public void setAppointmentService(AppointmentService appointmentService) {
		this.appointmentService = appointmentService;
	}

}
