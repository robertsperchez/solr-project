package ro.kronsoft.practice.controllers;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.view.ViewScoped;

import org.primefaces.PrimeFaces;
import org.springframework.context.annotation.ComponentScan;

import ro.kronsoft.practice.dto.PatientDto;
import ro.kronsoft.practice.models.LazyPatientAppointmentsModel;
import ro.kronsoft.practice.models.LazyPatientModel;
import ro.kronsoft.practice.models.PatientModel;
import ro.kronsoft.practice.services.AppointmentService;
import ro.kronsoft.practice.services.PatientService;
import ro.kronsoft.practice.transformers.PatientTransformer;

@ManagedBean(name = "patientController")
@ViewScoped
@ComponentScan("ro.kronsoft.practice")
public class PatientController {

	@ManagedProperty(value = "#{patientService}")
	private PatientService patientService;

	@ManagedProperty(value = "#{patientTransformer}")
	private PatientTransformer patientTransformer;

	@ManagedProperty(value = "#{appointmentService}")
	private AppointmentService appointmentService;

	@ManagedProperty(value = "#{patientModel}")
	private PatientModel patientModel;

	@ManagedProperty(value = "#{lazyPatientModel}")
	private LazyPatientModel lazyPatientModel;

	@ManagedProperty(value = "#{lazyPatientAppointmentsModel}")
	private LazyPatientAppointmentsModel lazyPatientAppointmentsModel;

	public PatientController() {
		// Empty constructor
	}

	@PostConstruct
	public void init() {
		lazyPatientModel.setPatientsCount(patientService.countAllPatients());
	}

	/**
	 * Function called when the add new patient button is clicked.
	 */
	public void addClicked() {
		patientModel.setPatient(new PatientDto());
		PrimeFaces current = PrimeFaces.current();
		current.executeScript("PF('patientFormDialog').show();");
	}

	/**
	 * Function called when the edit patient button is clicked.
	 * 
	 * @param doctor The patient that is chosen to be edited.
	 */
	public void editClicked(final PatientDto patient) {
		patientModel.setPatient(patient);
		PrimeFaces current = PrimeFaces.current();
		current.executeScript("PF('patientFormDialog').show();");
	}

	/**
	 * Function called when the view patient button is clicked.
	 * 
	 * @param doctor The patient that is chosen to be viewed.
	 */
	public void viewClicked(final PatientDto patient) {
		patientModel.setPatient(patient);
		lazyPatientAppointmentsModel.setPatientId(patient.getId());
		PrimeFaces current = PrimeFaces.current();
		current.executeScript("PF('patientsProfileDialog').show();");
	}

	/**
	 * Function called when the save appointment button is clicked.
	 */
	public void createPatient() {
		if (patientModel.getPatient().getId().isEmpty()) {
			patientModel.getPatient().setId(null);
		}
		patientService.savePatient(patientTransformer.toEntity(patientModel.getPatient()));
		lazyPatientModel.setPatientsCount(patientService.countAllPatients());
		PrimeFaces current = PrimeFaces.current();
		current.executeScript("PF('patientFormDialog').hide();");
	}

	/**
	 * Function called when the delete patient button is clicked.
	 * 
	 * @param id The id of the patient that will be deleted.
	 */
	public void deletePatient(final String id) {
		patientService.deletePatient(id);
		appointmentService.deleteAllByPatientId(id);
		lazyPatientModel.setPatientsCount(patientService.countAllPatients());
	}

	public LazyPatientModel getLazyPatientModel() {
		return lazyPatientModel;
	}

	public AppointmentService getAppointmentService() {
		return appointmentService;
	}

	public void setAppointmentService(AppointmentService appointmentService) {
		this.appointmentService = appointmentService;
	}

	public void setLazyPatientModel(LazyPatientModel lazyPatientModel) {
		this.lazyPatientModel = lazyPatientModel;
	}

	public PatientService getPatientService() {
		return patientService;
	}

	public void setPatientService(PatientService patientService) {
		this.patientService = patientService;
	}

	public PatientModel getPatientModel() {
		return patientModel;
	}

	public void setPatientModel(PatientModel patientModel) {
		this.patientModel = patientModel;
	}

	public LazyPatientAppointmentsModel getLazyPatientAppointmentsModel() {
		return lazyPatientAppointmentsModel;
	}

	public void setLazyPatientAppointmentsModel(LazyPatientAppointmentsModel lazyPatientAppointmentsModel) {
		this.lazyPatientAppointmentsModel = lazyPatientAppointmentsModel;
	}

	public PatientTransformer getPatientTransformer() {
		return patientTransformer;
	}

	public void setPatientTransformer(PatientTransformer patientTransformer) {
		this.patientTransformer = patientTransformer;
	}

}
