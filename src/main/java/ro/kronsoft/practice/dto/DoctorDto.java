package ro.kronsoft.practice.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import ro.kronsoft.practice.entities.enums.SpecialtyType;

public class DoctorDto extends BaseDto {

	private static final long serialVersionUID = -8204532481762773584L;

	@NotNull
	@NotBlank
	@Size(min = 2, max = 64)
	@Pattern(regexp = "^[A-Za-z]+[,.'-]*[A-Za-z]$")
	private String firstName;

	@NotNull
	@NotBlank
	@Size(min = 2, max = 64)
	@Pattern(regexp = "^[A-Za-z]+[,.'-]*[A-Za-z]$")
	private String lastName;

	@NotNull
	@NotBlank
	@Size(min = 2, max = 64)
	private String fullName;

	@NotNull
	private SpecialtyType specialtyType;

	@NotNull
	@NotBlank
	@Size(min = 0, max = 32)
	@Pattern(regexp = "[0-9]+")
	private String phoneNumber;

	@Email
	@NotNull
	@NotBlank
	private String email;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public SpecialtyType getSpecialtyType() {
		return specialtyType;
	}

	public void setSpecialtyType(SpecialtyType specialtyType) {
		this.specialtyType = specialtyType;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public DoctorDto() {
		super();
	}

	public DoctorDto(
			@NotNull @NotBlank @Size(min = 2, max = 64) @Pattern(regexp = "^[A-Za-z]+[,.'-]*[A-Za-z]$") String firstName,
			@NotNull @NotBlank @Size(min = 2, max = 64) @Pattern(regexp = "^[A-Za-z]+[,.'-]*[A-Za-z]$") String lastName,
			@NotNull @NotBlank @Size(min = 2, max = 64) String fullName, @NotNull SpecialtyType specialtyType,
			@NotNull @NotBlank @Size(min = 0, max = 32) @Pattern(regexp = "[0-9]+") String phoneNumber,
			@Email @NotNull @NotBlank String email) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.specialtyType = specialtyType;
		this.phoneNumber = phoneNumber;
		this.email = email;
	}

	@Override
	public String toString() {
		return "DoctorDto [firstName=" + firstName + ", lastName=" + lastName + ", fullName=" + fullName
				+ ", specialtyType=" + specialtyType + ", phoneNumber=" + phoneNumber + ", email=" + email + "]";
	}

}
