package ro.kronsoft.practice.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

public class BaseDto implements Serializable {

	private static final long serialVersionUID = -7854715654290433143L;

	@NotNull
	protected String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
