package ro.kronsoft.practice.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;

import ro.kronsoft.practice.entities.enums.AppointmentStatus;
import ro.kronsoft.practice.entities.enums.AppointmentType;
import ro.kronsoft.practice.entities.enums.SpecialtyType;

public class AppointmentDto extends BaseDto {

	private static final long serialVersionUID = -7506846395522942572L;

	@NotNull
	private AppointmentType appointmentType;

	@NotNull
	private AppointmentStatus appointmentStatus;

	@NotNull
	private SpecialtyType specialtyType;

	@NotNull
	private LocalDate date;

	@NotNull
	@JsonFormat(pattern = "HH:mm")
	private String startTime;

	@NotNull
	@JsonFormat(pattern = "HH:mm")
	private String endTime;

	@Size(min = 0, max = 512)
	private String description;

	@NotNull
	private String patientId;

	@NotNull
	private String doctorId;

	public AppointmentType getAppointmentType() {
		return appointmentType;
	}

	public void setAppointmentType(AppointmentType appointmentType) {
		this.appointmentType = appointmentType;
	}

	public AppointmentStatus getAppointmentStatus() {
		return appointmentStatus;
	}

	public void setAppointmentStatus(AppointmentStatus appointmentStatus) {
		this.appointmentStatus = appointmentStatus;
	}

	public SpecialtyType getSpecialtyType() {
		return specialtyType;
	}

	public void setSpecialtyType(SpecialtyType specialtyType) {
		this.specialtyType = specialtyType;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public String getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(String doctorId) {
		this.doctorId = doctorId;
	}

	public AppointmentDto() {
		super();
	}

	public AppointmentDto(@NotNull AppointmentType appointmentType, @NotNull AppointmentStatus appointmentStatus,
			@NotNull SpecialtyType specialtyType, @NotNull LocalDate date, @NotNull String startTime,
			@NotNull String endTime, @Size(min = 0, max = 512) String description, @NotNull String patientId,
			@NotNull String doctorId) {
		super();
		this.appointmentType = appointmentType;
		this.appointmentStatus = appointmentStatus;
		this.specialtyType = specialtyType;
		this.date = date;
		this.startTime = startTime;
		this.endTime = endTime;
		this.description = description;
		this.patientId = patientId;
		this.doctorId = doctorId;
	}

	@Override
	public String toString() {
		return "AppointmentDto [appointmentType=" + appointmentType + ", appointmentStatus=" + appointmentStatus
				+ ", specialtyType=" + specialtyType + ", date=" + date + ", startTime=" + startTime + ", endTime="
				+ endTime + ", description=" + description + ", patientId=" + patientId + ", doctorId=" + doctorId
				+ "]";
	}

}
