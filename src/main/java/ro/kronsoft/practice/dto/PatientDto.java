package ro.kronsoft.practice.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import ro.kronsoft.practice.entities.enums.SexTypeEnum;

public class PatientDto extends BaseDto {

	private static final long serialVersionUID = -7707373096689375626L;

	@NotNull
	@NotBlank
	@Size(min = 2, max = 64)
	@Pattern(regexp = "^[A-Za-z]+[,.'-]*[A-Za-z]$")
	private String firstName;

	@NotNull
	@NotBlank
	@Size(min = 2, max = 64)
	@Pattern(regexp = "^[A-Za-z]+[,.'-]*[A-Za-z]$")
	private String lastName;

	@NotNull
	@NotBlank
	@Size(min = 2, max = 64)
	private String fullName;

	@NotNull
	@NotBlank
	@Size(min = 13, max = 13)
	@Pattern(regexp = "^[0-9]*$")
	private String cnp;

	@NotNull
	private SexTypeEnum sexType;

	@NotNull
	private LocalDate birthDate;

	@NotNull
	@NotBlank
	@Size(min = 0, max = 32)
	@Pattern(regexp = "[0-9]+")
	private String phoneNumber;

	@Size(min = 0, max = 32)
	private String country;

	@Size(min = 0, max = 32)
	private String city;

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCnp() {
		return cnp;
	}

	public void setCnp(String cnp) {
		this.cnp = cnp;
	}

	public SexTypeEnum getSexType() {
		return sexType;
	}

	public void setSexType(SexTypeEnum sexType) {
		this.sexType = sexType;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public PatientDto() {
		super();
	}

	public PatientDto(
			@NotNull @NotBlank @Size(min = 2, max = 64) @Pattern(regexp = "^[A-Za-z]+[,.'-]*[A-Za-z]$") String firstName,
			@NotNull @NotBlank @Size(min = 2, max = 64) @Pattern(regexp = "^[A-Za-z]+[,.'-]*[A-Za-z]$") String lastName,
			@NotNull @NotBlank @Size(min = 2, max = 64) String fullName,
			@NotNull @NotBlank @Size(min = 13, max = 13) @Pattern(regexp = "^[0-9]*$") String cnp,
			@NotNull SexTypeEnum sexType, @NotNull LocalDate birthDate,
			@NotNull @NotBlank @Size(min = 0, max = 32) @Pattern(regexp = "[0-9]+") String phoneNumber,
			@Size(min = 0, max = 32) String country, @Size(min = 0, max = 32) String city) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.cnp = cnp;
		this.sexType = sexType;
		this.birthDate = birthDate;
		this.phoneNumber = phoneNumber;
		this.country = country;
		this.city = city;
	}

	@Override
	public String toString() {
		return "PatientDto [firstName=" + firstName + ", lastName=" + lastName + ", fullName=" + fullName + ", cnp="
				+ cnp + ", sexType=" + sexType + ", birthDate=" + birthDate + ", phoneNumber=" + phoneNumber
				+ ", country=" + country + ", city=" + city + "]";
	}

}
