package ro.kronsoft.practice.validators;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import ro.kronsoft.practice.services.PatientService;

@ManagedBean(name = "cnpValidator")
public class CNPValidator implements Validator {

	@ManagedProperty(value = "#{patientService}")
	private PatientService patientService;

	public PatientService getPatientService() {
		return patientService;
	}

	public void setPatientService(PatientService patientService) {
		this.patientService = patientService;
	}

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) {
		UIInput idInput = (UIInput) component.getAttributes().get("idAttribute");
		String patientId = idInput.getSubmittedValue().toString();
		String cnp = value.toString();

		boolean uniqueCnp = patientId.isEmpty() ? patientService.findPatientByCnp(cnp) == null
				: patientService.findByCnpNotId(cnp, patientId) == null;

		if (!uniqueCnp) {
			FacesMessage msg = new FacesMessage("CNP validation failed!", "A patient with that CNP already exists!");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}
	}
}
