package ro.kronsoft.practice.validators;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import ro.kronsoft.practice.services.DoctorService;

@ManagedBean(name = "emailValidator")
public class EmailValidator implements Validator {

	@ManagedProperty(value = "#{doctorService}")
	private DoctorService doctorService;

	public DoctorService getDoctorService() {
		return doctorService;
	}

	public void setDoctorService(DoctorService doctorService) {
		this.doctorService = doctorService;
	}

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) {
		UIInput idInput = (UIInput) component.getAttributes().get("idAttribute");
		String doctorId = idInput.getSubmittedValue().toString();
		String email = value.toString();

		boolean uniqueEmail = doctorId.isEmpty() ? doctorService.findDoctorByEmail(email) == null
				: doctorService.findByEmailNotId(email, doctorId) == null;

		if (!uniqueEmail) {
			FacesMessage msg = new FacesMessage("Email validation failed!", "A doctor with that email already exists!");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}
	}
}
