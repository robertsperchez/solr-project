package ro.kronsoft.practice.validators;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import ro.kronsoft.practice.entities.Doctor;
import ro.kronsoft.practice.services.DoctorService;

@ManagedBean(name = "specialtyValidator")
public class SpecialtyValidator implements Validator {

	@ManagedProperty(value = "#{doctorService}")
	private DoctorService doctorService;

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) {
		UIInput doctorInput = (UIInput) component.getAttributes().get("doctorValidator");

		if (doctorInput.getSubmittedValue() != null) {
			Doctor doctor = doctorService.findDoctorById(doctorInput.getSubmittedValue().toString());
			if (!doctor.getSpecialtyType().toString().equals(value.toString())) {
				FacesMessage msg = new FacesMessage("Specialty validation failed!",
						"This doctor doesn't have the " + value.toString() + " specialty!");
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				throw new ValidatorException(msg);
			}
		}
	}

	public DoctorService getDoctorService() {
		return doctorService;
	}

	public void setDoctorService(DoctorService doctorService) {
		this.doctorService = doctorService;
	}

}
