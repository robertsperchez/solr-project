package ro.kronsoft.practice.validators;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import ro.kronsoft.practice.entities.Appointment;
import ro.kronsoft.practice.services.AppointmentService;

@ManagedBean(name = "timeValidator")
public class TimeValidator implements Validator {

	static final String errorSummary = "Time validation failed!";
	static final String errorDetail = "There is already an appointment for that doctor at that time!";

	@ManagedProperty(value = "#{appointmentService}")
	private AppointmentService appointmentService;

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) {
		UIInput endTimeInput = (UIInput) component.getAttributes().get("endTimeValidator");
		UIInput dateInput = (UIInput) component.getAttributes().get("dateValidator");
		UIInput doctorInput = (UIInput) component.getAttributes().get("doctorValidator");
		UIInput patientInput = (UIInput) component.getAttributes().get("patValidator");

		LocalTime startTime = null, endTime = null;
		try {
			endTime = LocalTime.parse(endTimeInput.getSubmittedValue().toString());
			startTime = LocalTime.parse(value.toString());
		} catch (Exception e) {
			FacesMessage msg = new FacesMessage(errorSummary, "Invalid time format or times not introduced!");
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}

		if (endTime != null) {
			if (startTime.isAfter(endTime)) {
				FacesMessage msg = new FacesMessage(errorSummary, "Start time can't be after end time!");
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				throw new ValidatorException(msg);
			}

			if (startTime.equals(endTime)) {
				FacesMessage msg = new FacesMessage(errorSummary, "Start time can't be equal to the end time!");
				msg.setSeverity(FacesMessage.SEVERITY_ERROR);
				throw new ValidatorException(msg);
			}

			String doctorId = doctorInput.getSubmittedValue().toString();
			if (!dateInput.getSubmittedValue().toString().isEmpty()) {
				LocalDate tmp = LocalDate.parse(dateInput.getSubmittedValue().toString());
				Instant date = tmp.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
				List<Appointment> appointmentList = appointmentService.findByDateAndDoctorId(date.toString(), doctorId);
				String patientId = patientInput.getSubmittedValue().toString();

				for (Appointment appointment : appointmentList) {
					if (!appointment.getPatientId().equals(patientId)) {
						if (startTime.equals(LocalTime.parse(appointment.getStartTime()))) {
							FacesMessage msg = new FacesMessage(errorSummary, errorDetail);
							msg.setSeverity(FacesMessage.SEVERITY_ERROR);
							throw new ValidatorException(msg);
						}
						if (startTime.isAfter(LocalTime.parse(appointment.getStartTime()))
								&& startTime.isBefore(LocalTime.parse(appointment.getEndTime()))) {
							FacesMessage msg = new FacesMessage(errorSummary, errorDetail);
							msg.setSeverity(FacesMessage.SEVERITY_ERROR);
							throw new ValidatorException(msg);
						}
						if (endTime.isAfter(LocalTime.parse(appointment.getStartTime()))
								&& endTime.isBefore(LocalTime.parse(appointment.getEndTime()))) {
							FacesMessage msg = new FacesMessage(errorSummary, errorDetail);
							msg.setSeverity(FacesMessage.SEVERITY_ERROR);
							throw new ValidatorException(msg);
						}
						if (startTime.isBefore(LocalTime.parse(appointment.getStartTime()))
								&& endTime.isAfter(LocalTime.parse(appointment.getEndTime()))) {
							FacesMessage msg = new FacesMessage(errorSummary, errorDetail);
							msg.setSeverity(FacesMessage.SEVERITY_ERROR);
							throw new ValidatorException(msg);
						}
					}
				}
			}
		}
	}

	public AppointmentService getAppointmentService() {
		return appointmentService;
	}

	public void setAppointmentService(AppointmentService appointmentService) {
		this.appointmentService = appointmentService;
	}

}
