package ro.kronsoft.practice.repository;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.query.Criteria;
import org.springframework.data.solr.core.query.SimpleQuery;
import org.springframework.stereotype.Repository;

import ro.kronsoft.practice.entities.Appointment;
import ro.kronsoft.practice.entities.Doctor;
import ro.kronsoft.practice.entities.Patient;

@Repository
public class CustomFilterRepositoryImpl implements CustomFilterRepository {

	@Autowired
	private SolrTemplate solrTemplate;

	static final String constFirstName = "firstName_s";
	static final String constLastName = "lastName_s";
	static final String constCnp = "cnp_s";
	static final String constSexType = "sexType_s";
	static final String constBirthDate = "birthDate_dt";
	static final String constPhoneNumber = "phoneNumber_s";
	static final String constCountry = "country_s";
	static final String constCity = "city_s";
	static final String constDate = "date_dt";
	static final String constAppointmentType = "appointmentType_s";
	static final String constAppointmentStatus = "appointmentStatus_s";
	static final String constSpecialtyType = "specialtyType_s";
	static final String constStartTime = "startTime_s";
	static final String constEndTime = "endTime_s";
	static final String constPatientId = "patientId_s";
	static final String constDoctorId = "doctorId_s";
	static final String constEmail = "email_s";

	@Override
	public List<Patient> filterPatients(final Map<String, Object> filterFields, final Map<String, String> sortFields,
			final Pageable pageable) {
		Criteria criteria = createPatientsCriteria(filterFields);
		SimpleQuery query = new SimpleQuery(criteria);
		query.setPageRequest(pageable);
		if (!sortFields.isEmpty()) {
			query.addSort(sortPatients(sortFields));
		}
		return solrTemplate.queryForPage("patients", query, Patient.class).getContent();
	}

	@Override
	public List<Appointment> filterAppointments(final Map<String, Object> filterFields,
			final Map<String, String> sortFields, final Pageable pageable) {
		Criteria criteria = createAppointmentsCriteria(filterFields);
		SimpleQuery query = new SimpleQuery(criteria);
		query.setPageRequest(pageable);
		if (!sortFields.isEmpty()) {
			query.addSort(sortAppointments(sortFields));
		}
		return solrTemplate.queryForPage("appointments", query, Appointment.class).getContent();
	}

	@Override
	public List<Doctor> filterDoctors(final Map<String, Object> filterFields, final Map<String, String> sortFields,
			final Pageable pageable) {
		Criteria criteria = createDoctorsCriteria(filterFields);
		SimpleQuery query = new SimpleQuery(criteria);
		query.setPageRequest(pageable);
		if (!sortFields.isEmpty()) {
			query.addSort(sortDoctors(sortFields));
		}
		return solrTemplate.queryForPage("doctors", query, Doctor.class).getContent();
	}

	/**
	 * Goes through the sortFields map and returns a Sort object based on it.
	 * 
	 * @param sortFields
	 * @return Sort object to be used on the SimpleQuery.
	 */
	private Sort sortPatients(final Map<String, String> sortFields) {
		for (Map.Entry<String, String> entry : sortFields.entrySet()) {
			if (("ASCENDING").equals(entry.getValue())) {
				return Sort.by(Sort.Direction.ASC, entry.getKey());
			} else {
				return Sort.by(Sort.Direction.DESC, entry.getKey());
			}
		}
		return Sort.by(Sort.Direction.ASC, constSpecialtyType);
	}

	/**
	 * Goes through the sortFields map and returns a Sort object based on it.
	 * 
	 * @param sortFields
	 * @return Sort object to be used on the SimpleQuery.
	 */
	private Sort sortAppointments(final Map<String, String> sortFields) {
		for (Map.Entry<String, String> entry : sortFields.entrySet()) {
			if (("ASCENDING").equals(entry.getValue())) {
				return Sort.by(Sort.Direction.ASC, entry.getKey());
			} else {
				return Sort.by(Sort.Direction.DESC, entry.getKey());
			}
		}
		return Sort.by(Sort.Direction.ASC, constAppointmentType);
	}

	/**
	 * Goes through the sortFields map and returns a Sort object based on it.
	 * 
	 * @param sortFields
	 * @return Sort object to be used on the SimpleQuery.
	 */
	private Sort sortDoctors(final Map<String, String> sortFields) {
		for (Map.Entry<String, String> entry : sortFields.entrySet()) {
			if (("ASCENDING").equals(entry.getValue())) {
				return Sort.by(Sort.Direction.ASC, entry.getKey());
			} else {
				return Sort.by(Sort.Direction.DESC, entry.getKey());
			}
		}
		return Sort.by(Sort.Direction.ASC, constLastName);
	}

	/**
	 * Goes through the filterFields map and returns a Criteria object based on it.
	 * 
	 * @param filterFields
	 * @return Returns a Criteria object that is used
	 */
	private Criteria createPatientsCriteria(final Map<String, Object> filterFields) {
		Criteria criteria = null;
		for (Map.Entry<String, Object> entry : filterFields.entrySet()) {
			if (!entry.getValue().toString().isEmpty()) {
				switch (entry.getKey()) {
				case "firstName":
					if (criteria == null) {
						criteria = new Criteria(constFirstName).startsWith(entry.getValue().toString());
					} else {
						criteria = criteria.and(new Criteria(constFirstName).startsWith(entry.getValue().toString()));
					}
					break;
				case "lastName":
					if (criteria == null) {
						criteria = new Criteria(constLastName).startsWith(entry.getValue().toString());
					} else {
						criteria = criteria.and(new Criteria(constLastName).startsWith(entry.getValue().toString()));
					}
					break;
				case "cnp":
					if (criteria == null) {
						criteria = new Criteria(constCnp).startsWith(entry.getValue().toString());
					} else {
						criteria = criteria.and(new Criteria(constCnp).startsWith(entry.getValue().toString()));
					}
					break;
				case "sexType":
					if (criteria == null) {
						criteria = new Criteria(constSexType).startsWith(entry.getValue().toString());
					} else {
						criteria = criteria.and(new Criteria(constSexType).startsWith(entry.getValue().toString()));
					}
					break;
				case "birthDate":
					LocalDate tmp = (LocalDate) entry.getValue();
					Instant date = tmp.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
					if (criteria == null) {
						criteria = new Criteria(constBirthDate).is(date.toString());
					} else {
						criteria = criteria.and(new Criteria(constBirthDate).is(date.toString()));
					}
					break;
				case "phoneNumber":
					if (criteria == null) {
						criteria = new Criteria(constPhoneNumber).startsWith(entry.getValue().toString());
					} else {
						criteria = criteria.and(new Criteria(constPhoneNumber).startsWith(entry.getValue().toString()));
					}
					break;
				case "country":
					if (criteria == null) {
						criteria = new Criteria(constCountry).startsWith(entry.getValue().toString());
					} else {
						criteria = criteria.and(new Criteria(constCountry).startsWith(entry.getValue().toString()));
					}
					break;
				case "city":
					if (criteria == null) {
						criteria = new Criteria(constCity).startsWith(entry.getValue().toString());
					} else {
						criteria = criteria.and(new Criteria(constCity).startsWith(entry.getValue().toString()));
					}
					break;
				default:
					break;
				}
			}
		}
		return criteria;
	}

	/**
	 * Goes through the filterFields map and returns a Criteria object based on it.
	 * 
	 * @param filterFields
	 * @return Returns a Criteria object that is used
	 */
	private Criteria createAppointmentsCriteria(final Map<String, Object> filterFields) {
		Criteria criteria = null;
		for (Map.Entry<String, Object> entry : filterFields.entrySet()) {
			if (!entry.getValue().toString().isEmpty()) {
				switch (entry.getKey()) {
				case "appointmentType":
					if (criteria == null) {
						criteria = new Criteria(constAppointmentType).startsWith(entry.getValue().toString());
					} else {
						criteria = criteria
								.and(new Criteria(constAppointmentType).startsWith(entry.getValue().toString()));
					}
					break;
				case "appointmentStatus":
					if (criteria == null) {
						criteria = new Criteria(constAppointmentStatus).startsWith(entry.getValue().toString());
					} else {
						criteria = criteria
								.and(new Criteria(constAppointmentStatus).startsWith(entry.getValue().toString()));
					}
					break;
				case "specialtyType":
					if (criteria == null) {
						criteria = new Criteria(constSpecialtyType).startsWith(entry.getValue().toString());
					} else {
						criteria = criteria
								.and(new Criteria(constSpecialtyType).startsWith(entry.getValue().toString()));
					}
					break;
				case "date":
					LocalDate tmp = (LocalDate) entry.getValue();
					Instant date = tmp.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
					if (criteria == null) {
						criteria = new Criteria(constDate).is(date.toString());
					} else {
						criteria = criteria.and(new Criteria(constDate).is(date.toString()));
					}
					break;
				case "startTime":
					if (criteria == null) {
						criteria = new Criteria(constStartTime).startsWith(entry.getValue().toString());
					} else {
						criteria = criteria.and(new Criteria(constStartTime).startsWith(entry.getValue().toString()));
					}
					break;
				case "endTime":
					if (criteria == null) {
						criteria = new Criteria(constEndTime).startsWith(entry.getValue().toString());
					} else {
						criteria = criteria.and(new Criteria(constEndTime).startsWith(entry.getValue().toString()));
					}
					break;
				case "patientId":
					if (criteria == null) {
						criteria = new Criteria(constPatientId).startsWith(entry.getValue().toString());
					} else {
						criteria = criteria.and(new Criteria(constPatientId).startsWith(entry.getValue().toString()));
					}
					break;
				case "doctorId":
					if (criteria == null) {
						criteria = new Criteria(constDoctorId).startsWith(entry.getValue().toString());
					} else {
						criteria = criteria.and(new Criteria(constDoctorId).startsWith(entry.getValue().toString()));
					}
					break;
				default:
					break;
				}
			}
		}
		return criteria;
	}

	/**
	 * Goes through the filterFields map and returns a Criteria object based on it.
	 * 
	 * @param filterFields
	 * @return Returns a Criteria object that is used
	 */
	private Criteria createDoctorsCriteria(final Map<String, Object> filterFields) {
		Criteria criteria = null;
		for (Map.Entry<String, Object> entry : filterFields.entrySet()) {
			if (!entry.getValue().toString().isEmpty()) {
				switch (entry.getKey()) {
				case "firstName":
					if (criteria == null) {
						criteria = new Criteria(constFirstName).startsWith(entry.getValue().toString());
					} else {
						criteria = criteria.and(new Criteria(constFirstName).startsWith(entry.getValue().toString()));
					}
					break;
				case "lastName":
					if (criteria == null) {
						criteria = new Criteria(constLastName).startsWith(entry.getValue().toString());
					} else {
						criteria = criteria.and(new Criteria(constLastName).startsWith(entry.getValue().toString()));
					}
					break;
				case "email":
					if (criteria == null) {
						criteria = new Criteria(constEmail).startsWith(entry.getValue().toString());
					} else {
						criteria = criteria.and(new Criteria(constEmail).startsWith(entry.getValue().toString()));
					}
					break;
				case "specialtyType":
					if (criteria == null) {
						criteria = new Criteria(constSpecialtyType).startsWith(entry.getValue().toString());
					} else {
						criteria = criteria
								.and(new Criteria(constSpecialtyType).startsWith(entry.getValue().toString()));
					}
					break;
				case "phoneNumber":
					if (criteria == null) {
						criteria = new Criteria(constPhoneNumber).startsWith(entry.getValue().toString());
					} else {
						criteria = criteria.and(new Criteria(constPhoneNumber).startsWith(entry.getValue().toString()));
					}
					break;
				default:
					break;
				}
			}
		}
		return criteria;
	}

}
