package ro.kronsoft.practice.repository;

import java.util.List;

import org.springframework.data.solr.repository.Query;
import org.springframework.data.solr.repository.SolrCrudRepository;
import org.springframework.stereotype.Repository;

import ro.kronsoft.practice.entities.Patient;

@Repository("patientRepository")
public interface PatientRepository extends CustomFilterRepository, SolrCrudRepository<Patient, Long> {

	List<Patient> findByFullNameStartingWith(String query);

	Patient findById(String id);

	void deleteById(String id);

	Patient findByCnp(String cnp);

	@Query("cnp_s:?0 AND -id:?1")
	Patient findByCnpNotId(String cnp, String id);

}
