package ro.kronsoft.practice.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.solr.repository.SolrCrudRepository;
import org.springframework.stereotype.Repository;

import ro.kronsoft.practice.entities.Appointment;

@Repository("appointmentRepository")
public interface AppointmentRepository extends CustomFilterRepository, SolrCrudRepository<Appointment, Long> {

	Appointment findById(String id);

	void deleteById(String id);

	void deleteAllByPatientId(String patientId);

	void deleteAllByDoctorId(String doctorId);

	List<Appointment> findAllByPatientId(String patientId, Pageable pageable);

	Long countByPatientId(String patientId);

	List<Appointment> findAllByDoctorId(String doctorId, Pageable pageable);

	Long countByDoctorId(String doctorId);

	List<Appointment> findByDateAndDoctorId(String date, String doctorId);
}
