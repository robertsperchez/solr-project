package ro.kronsoft.practice.repository;

import java.util.List;

import org.springframework.data.solr.repository.Query;
import org.springframework.data.solr.repository.SolrCrudRepository;
import org.springframework.stereotype.Repository;

import ro.kronsoft.practice.entities.Doctor;
import ro.kronsoft.practice.entities.enums.SpecialtyType;

@Repository("doctorRepository")
public interface DoctorRepository extends CustomFilterRepository, SolrCrudRepository<Doctor, Long> {
	
	@Query("fullName_s:?0* AND specialtyType_s:?1")
	List<Doctor> findByFullNameAndSpecialty(String query, SpecialtyType specialtyType);
	
	List<Doctor> findByFullNameStartingWith(String query);
	
	Doctor findById(String id);
	
	Doctor findByEmail(String email);
	
	void deleteById(String id);
	
	@Query("email_s:?0 AND -id:?1")
	Doctor findByEmailNotId(String email, String id);
	
	Doctor findByFullName(String fullName);
}
