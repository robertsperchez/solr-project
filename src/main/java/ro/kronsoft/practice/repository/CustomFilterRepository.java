package ro.kronsoft.practice.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Pageable;

import ro.kronsoft.practice.entities.Appointment;
import ro.kronsoft.practice.entities.Doctor;
import ro.kronsoft.practice.entities.Patient;

public interface CustomFilterRepository {

	/**
	 * Sends a query to solr based on user selected filters and sorts.
	 * 
	 * @param filterFields
	 * @param sortFields
	 * @param pageable     Used for lazy loading.
	 * @return Returns the filtered patients from solr.
	 */
	List<Patient> filterPatients(final Map<String, Object> filterFields, final Map<String, String> sortFields,
			final Pageable pageable);

	/**
	 * Sends a query to solr based on user selected filters and sorts.
	 * 
	 * @param filterFields
	 * @param sortFields
	 * @param pageable     Used for lazy loading.
	 * @return Returns the filtered appointments from solr.
	 */
	List<Appointment> filterAppointments(final Map<String, Object> filterFields, final Map<String, String> sortFields,
			final Pageable pageable);

	/**
	 * Sends a query to solr based on user selected filters and sorts.
	 * 
	 * @param filterFields
	 * @param sortFields
	 * @param pageable     Used for lazy loading.
	 * @return Returns the filtered doctors from solr.
	 */
	List<Doctor> filterDoctors(final Map<String, Object> filterFields, final Map<String, String> sortFields,
			final Pageable pageable);
}
