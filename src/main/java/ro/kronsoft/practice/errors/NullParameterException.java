package ro.kronsoft.practice.errors;

public class NullParameterException extends BaseException {

	private static final long serialVersionUID = -8765040254074495113L;

	public NullParameterException(String errorMessage) {
		super("The parameter is null: " + errorMessage);
	}
}
