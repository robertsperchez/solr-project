package ro.kronsoft.practice.errors;

public class BaseException extends Exception {

	private static final long serialVersionUID = 4500696747521547364L;

	public BaseException(String errorMessage) {
		super(errorMessage);
	}
	
}
