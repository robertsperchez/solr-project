package ro.kronsoft.practice.transformers;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ro.kronsoft.practice.dto.DoctorDto;
import ro.kronsoft.practice.entities.Doctor;
import ro.kronsoft.practice.repository.DoctorRepository;

@Component
public class DoctorTransformer extends AbstractTransformer<Doctor, DoctorDto> {

	@Autowired
	private DoctorRepository doctorRepository;

	@Override
	public DoctorDto toDto(final Doctor doctor) {
		DoctorDto dto = new DoctorDto();
		BeanUtils.copyProperties(doctor, dto);
		return dto;
	}

	@Override
	public Doctor toEntity(final DoctorDto dto) {
		Doctor doctor = findOrCreateNew(dto.getId());
		BeanUtils.copyProperties(dto, doctor);
		return doctor;
	}

	@Override
	protected Doctor findOrCreateNew(final String id) {
		return id == null ? new Doctor() : Optional.of(doctorRepository.findById(id)).orElseGet(Doctor::new);
	}

}
