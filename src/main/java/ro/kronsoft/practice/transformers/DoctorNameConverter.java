package ro.kronsoft.practice.transformers;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import ro.kronsoft.practice.dto.DoctorDto;
import ro.kronsoft.practice.services.DoctorService;

@ManagedBean(name = "doctorNameConverter")
@FacesConverter(value = "doctorNameConverter")
public class DoctorNameConverter implements Converter {

	@ManagedProperty(value = "#{doctorService}")
	private DoctorService doctorService;
	
	@ManagedProperty(value = "#{doctorTransformer}")
	private DoctorTransformer doctorTransformer;

	public DoctorTransformer getDoctorTransformer() {
		return doctorTransformer;
	}

	public void setDoctorTransformer(DoctorTransformer doctorTransformer) {
		this.doctorTransformer = doctorTransformer;
	}

	public DoctorService getDoctorService() {
		return doctorService;
	}

	public void setDoctorService(DoctorService doctorService) {
		this.doctorService = doctorService;
	}

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		return value != null ? doctorTransformer.toDto(doctorService.findDoctorById(value)) : null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof DoctorDto) {
			DoctorDto doctor = (DoctorDto) value;
			return doctor.getId();
		} else if (value != null) {
			throw new RuntimeException("Bad implementation! Expected value should be a doctor!" + value);
		}
		return null;
	}

}
