package ro.kronsoft.practice.transformers;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ro.kronsoft.practice.dto.PatientDto;
import ro.kronsoft.practice.entities.Patient;
import ro.kronsoft.practice.repository.PatientRepository;

//@ManagedBean(name = "patientTransformer", eager = true)
@Component
public class PatientTransformer extends AbstractTransformer<Patient, PatientDto> {

	@Autowired
	private PatientRepository patientRepository;

	@Override
	public PatientDto toDto(final Patient patient) {
		PatientDto dto = new PatientDto();
		BeanUtils.copyProperties(patient, dto);
		return dto;
	}

	@Override
	public Patient toEntity(final PatientDto dto) {
		Patient patient = findOrCreateNew(dto.getId());
		BeanUtils.copyProperties(dto, patient);
		return patient;
	}

	@Override
	protected Patient findOrCreateNew(final String id) {
		return id == null ? new Patient() : Optional.of(patientRepository.findById(id)).orElseGet(Patient::new);
	}

}
