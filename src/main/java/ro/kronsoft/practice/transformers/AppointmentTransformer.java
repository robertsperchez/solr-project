package ro.kronsoft.practice.transformers;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ro.kronsoft.practice.dto.AppointmentDto;
import ro.kronsoft.practice.entities.Appointment;
import ro.kronsoft.practice.repository.AppointmentRepository;

@Component
public class AppointmentTransformer extends AbstractTransformer<Appointment, AppointmentDto> {

	@Autowired
	private AppointmentRepository appointmentRepository;

	@Override
	public AppointmentDto toDto(final Appointment appointment) {
		AppointmentDto dto = new AppointmentDto();
		BeanUtils.copyProperties(appointment, dto);
		return dto;
	}

	@Override
	public Appointment toEntity(final AppointmentDto dto) {
		Appointment appointment = findOrCreateNew(dto.getId());
		BeanUtils.copyProperties(dto, appointment);
		return appointment;
	}

	@Override
	protected Appointment findOrCreateNew(final String id) {
		return id == null ? new Appointment()
				: Optional.of(appointmentRepository.findById(id)).orElseGet(Appointment::new);
	}

}
