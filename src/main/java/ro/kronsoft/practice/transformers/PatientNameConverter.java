package ro.kronsoft.practice.transformers;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import ro.kronsoft.practice.dto.PatientDto;
import ro.kronsoft.practice.services.PatientService;

@ManagedBean(name = "patientNameConverter")
@FacesConverter(value = "patientNameConverter")
public class PatientNameConverter implements Converter {

	@ManagedProperty(value = "#{patientService}")
	private PatientService patientService;

	@ManagedProperty(value = "#{patientTransformer}")
	private PatientTransformer patientTransformer;

	public PatientTransformer getPatientTransformer() {
		return patientTransformer;
	}

	public void setPatientTransformer(PatientTransformer patientTransformer) {
		this.patientTransformer = patientTransformer;
	}

	public PatientService getPatientService() {
		return patientService;
	}

	public void setPatientService(PatientService patientService) {
		this.patientService = patientService;
	}

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		return value != null ? patientTransformer.toDto(patientService.getPatientById(value)) : null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof PatientDto) {
			PatientDto patient = (PatientDto) value;
			return patient.getId();
		} else if (value != null) {
			throw new RuntimeException("Bad implementation! Expected value should be a patient!" + value);
		}
		return null;
	}

}
