package ro.kronsoft.practice.transformers;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import ro.kronsoft.practice.entities.Patient;
import ro.kronsoft.practice.services.PatientService;

@ManagedBean(name = "patientIdConverter")
@FacesConverter(value = "patientIdConverter")
public class PatientIdConverter implements Converter {

	@ManagedProperty(value = "#{patientService}")
	private PatientService patientService;

	public PatientService getPatientService() {
		return patientService;
	}

	public void setPatientService(PatientService patientService) {
		this.patientService = patientService;
	}

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		return value != null ? patientService.getPatientById(value) : null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			Patient patient = patientService.getPatientById(value.toString());
			return String.valueOf(patient.getFirstName() + " " + patient.getLastName());
		}
		return null;
	}

}
