package ro.kronsoft.practice.transformers;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import ro.kronsoft.practice.dto.BaseDto;
import ro.kronsoft.practice.entities.BaseEntity;

public abstract class AbstractTransformer<T extends BaseEntity, Y extends BaseDto> {

	/**
	 * Method used to transform an entity to a data transfer object to be exposed to
	 * the user.
	 * 
	 * @param entity The entity to be transformed.
	 * @return Returns the transformed DTO.
	 */
	public abstract Y toDto(T entity);

	/**
	 * Method used to transform a data transfer object into an entity to be used in
	 * the backend.
	 * 
	 * @param dto The DTO to be transformed.
	 * @return Returns the transformed entity.
	 */
	public abstract T toEntity(Y dto);

	/**
	 * Checks the id to find a corresponding entity in the backend, otherwise
	 * returns null.
	 * 
	 * @param id
	 * @return Returns an entity if one is found in the solr database, otherwise
	 *         returns null.
	 */
	protected abstract T findOrCreateNew(String id);

	/**
	 * Transforms a list of entities into a list of DTO's to be exposed to the user.
	 * Useful for showing datatables.
	 * 
	 * @param entityList The list of entities to be transformed.
	 * @return Returns the transformed list of DTO's.
	 */
	public List<Y> toDtoList(List<T> entityList) {
		if (entityList == null) {
			return Collections.emptyList();
		}
		return entityList.stream().map(this::toDto).collect(Collectors.toList());
	}

}
